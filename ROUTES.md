Id | Module | Path | Description
--- | --- | ---| ---
1 | Newsystem | /newsystem | مدیریت ویژگی ها
2 | Signin | /signin | ورود به برنامه
3 | ContactUs | /contact-us |ارتباط با ما      
4 | Faq | /faq | سوالات متداول
5 | PrivacyPolicy | /privacy-policy | حریم شخصی
6 | Profile | /profile | پروفایل
7 | ForgetPassword | /forget-password | مدیریت فراموشی اطلاعات اکانت