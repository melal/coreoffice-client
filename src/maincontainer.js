import React from "react";
import Routs from "./Routes";
import { Link } from "react-router-dom";
import Container from "@material-ui/core/Container";
import { makeStyles, useTheme, fade } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import StickyFooter from "./Modules/MainContainer/StickyFooter";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";
import UserToolInAppbar from "./config/Components/UserToolInAppbar";
import Icons from "./assets/images/icons/Icons";
import clsx from "clsx";
import "./content/css/mainContainer.css";
import "./content/css/footer.css";
import "./content/css/fonts.css";
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu";
import { addToCustomSideMenu } from "./actions";
import { connect } from "react-redux";
import configs from "./config/Config";
// import { config } from "@fortawesome/fontawesome-svg-core";

function MainContainer(props) {
  const [isGoToSearchProcess, setIsGoToSearchProcess] = React.useState(false);
  const [searchValue, setSearchValue] = React.useState();
  const theme = useTheme();
  //const [open, setOpen] = React.useState(false);
  const [main_Drawer, setDrawer] = React.useState(false);
  const open = false;

  function setSearchContent(e) {
    setSearchValue(e.target.value);
  }

  function redirectToSearchPage(e) {
    switch (e.keyCode) {
      case 13:
        window.location.href = `/#/search/${searchValue}`;
    }
  }
  function toggleDrawer(event, side, state_drawer) {
    setDrawer(state_drawer);
  }

  function addToCustomMenu(e, data) {
    console.info("e:", e);
    console.info("data:", data);
    props.dispatch(
      addToCustomSideMenu({
        text: data.target.textContent,
        link: "http://www.google.com",
      })
    );
  }

  function handleClick(e, data) {
    console.info("umad");
  }

  return (
    <div>
      <AppBar position="fixed" className="appBar">
        <Toolbar className="toolbar">
          {props.isLoggedIn ? <DynamicIcon /> : null}

          <div style={{ display: "block", flexGrow: 1 }}> </div>
          <div edge="start">
            <div>
              <SearchIcon />
            </div>
            <InputBase
              onChange={(e) => setSearchContent(e)}
              onKeyDown={(e) => redirectToSearchPage(e)}
              placeholder="جستجو کنید"
              inputProps={{
                "aria-label": "search",
                style: {
                  fontFamily: "IranSans_Light",
                  fontSize: "13px",
                },
              }}
            />
          </div>
          <UserToolInAppbar edge="start" />
        </Toolbar>
      </AppBar>

      <div class="mainHeader">
        <div className="context">
          <span className="title">سامانه متمرکز اداری </span>
          <br />
          <span className="company">
            شرکت تجارت الکترونیک و فناوری اطلاعات ملل
          </span>
          <div className="logo">
            <img src="../../assets/image/fam-logo.png" height="35" />
            <br />
            <span>{configs.Version}</span>
          </div>
        </div>
      </div>

      <SwipeableDrawer
        // anchor="left"
        open={main_Drawer}
        onClose={(event) => toggleDrawer(event, "left", false)}
        onOpen={(event) => toggleDrawer(event, "left", true)}
      >
        <div>
          <IconButton>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <List>
          {props.menu.map((text, index) => (
            <React.Fragment key={index}>
              <ContextMenuTrigger id={"contextmenu" + index}>
                <ListItem
                  button
                  key={index}
                  component={Link}
                  to={text.link}
                  onClick={(event) => toggleDrawer(event, "left", false)}
                >
                  <ListItemIcon>
                    {text.icon}
                  </ListItemIcon>
                  <ListItemText
                    primary={
                      <Typography>
                        {text.text}
                      </Typography>
                    }
                  />
                </ListItem>
              </ContextMenuTrigger>
              <ContextMenu
                id={"contextmenu" + index}
                style={{
                  padding: "10px 10px",
                  direction: "rtl",
                  backgroundColor: "#ececec",
                  position: "fixed",
                  opacity: "1",
                  pointerEvents: "auto",
                  top: "79px",
                  left: "87px",
                  borderRadius: "3px",
                  zIndex: "1",
                }}
              >
                <MenuItem
                  data={{ foo: text.link }}
                  onClick={addToCustomMenu}
                  style={{
                    padding: "6px 1px",
                    cursor: "pointer",
                  }}
                >
                  <Typography
                    style={{
                      fontFamily: "IranSans",
                    }}
                  >
                    افزودن{" "}
                    <span
                      style={{
                        fontWeight: "bold",
                      }}
                    >
                      {text.text}
                    </span>{" "}
                    به لیست سریع
                  </Typography>
                </MenuItem>
                <Divider />
                <MenuItem
                  data={{ foo: "bar" }}
                  onClick={handleClick}
                  style={{
                    padding: "6px 1px",
                  }}
                >
                  <Typography
                    style={{
                      fontFamily: "IranSans",
                    }}
                  >
                    ContextMenu Item 2
                  </Typography>
                </MenuItem>
                <MenuItem divider />
                <MenuItem
                  data={{ foo: "bar" }}
                  onClick={handleClick}
                  style={{
                    padding: "6px 1px",
                  }}
                >
                  <Typography
                    style={{
                      fontFamily: "IranSans",
                    }}
                  >
                    ContextMenu Item 3
                  </Typography>
                </MenuItem>
              </ContextMenu>
            </React.Fragment>
          ))}
        </List>

        <Divider />
        <List>
          {[
            {
              icon: <Icons icon="Shoppingplan" />,
              text: "همکاران",
              link: "/shopping-plans",
            },
            { icon: <Icons icon="Goal" />, text: "هدف ما", link: "/about" },
          ].map((text, index) => (
            <ListItem
              button
              key={index}
              component={Link}
              to={text.link}
              onClick={(event) => toggleDrawer(event, "left", false)}
            >
              <ListItemIcon>
              </ListItemIcon>
              <ListItemText
                primary={
                  <Typography
                    style={{
                      fontFamily: "IranSans",
                      fontSize: "12px",
                    }}
                  >
                    {text.text}
                  </Typography>
                }
              />
            </ListItem>
          ))}
        </List>
      </SwipeableDrawer>
      <Container className="container">
        <Routs />
      </Container>
      <StickyFooter />
    </div>
  );

  function DynamicIcon(props) {
    return (
      <IconButton
        color="inherit"
        onClick={(event) => toggleDrawer(event, "left", true)}
        edge="start"
      >
        <MenuIcon />
      </IconButton>
    );
  }
}
const mapStateToProps = (state) => {
  const token = state.token;
  const isLoggedIn = state.isLoggedIn;
  let menu = state.menu;
  let temp = [...menu];
  menu = [];
  temp.forEach((value, index) => {
    menu.push({
      icon: <Icons />,
      text: value,
      link: `/${value}`,
    });
  });

  return {
    token,
    isLoggedIn,
    menu,
  };
};

export default connect(mapStateToProps)(MainContainer);
