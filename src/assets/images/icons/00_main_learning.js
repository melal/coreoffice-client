import React from "react";
import Icon from "./svg_00_main_learning.svg"
class svg_00_main_learning extends React.Component {
	constructor(props) {
		super(props);
	}
	render(){
		var width = 50;
		var height = 50;
		if(this.props.size != "" && this.props.size != undefined ){
			width  = this.props.size;
			height = this.props.size;
		}
		if(this.props.width != "" && this.props.width != undefined ){
			width = this.props.width;
		}
		console.log(this.props.height != "" && this.props.height != undefined );
		if(this.props.height != "" && this.props.height != undefined ){
			height = this.props.height;
		}
		return(
			<div style={{
				backgroundImage: `url(${Icon})`, 
				width:width+"px",
				height:height+"px",
				display:"inline-block",
				backgroundSize: "contain",
				backgroundPosition: "center",
				backgroundRepeat: "no-repeat",
				}}>
			</div>
		)
	}
}
export default svg_00_main_learning;