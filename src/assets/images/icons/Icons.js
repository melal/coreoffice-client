import React from "react";
import Icon from "./svg_00_main_learning.svg"
import Learning from "./svg_00_main_learning.svg"
import Test from "./svg_01_main_test.svg"
import Book from "./svg_02_definitions.svg"
import Beyond from "./beyond.svg"
import Science from "./science.png"
import ScienceSidebar from "./science.svg"
import RequestSide from "./requests.svg"
import Request from "./requestn.png"
import Goal from "./goal.svg"
import Shoppingplan from "./shoppingplan.svg"
import Finance from "./finance.png"
import Security from "./security.svg"
class svg_01_main_test extends React.Component {

	render(){
		var width = 30;
		var height = 30;
		var components = {
			"Learning": Learning,
			"Test": Test,
			"Book": Book,
			"Beyond": Beyond,
			"Science": Science,
			"Request": Request,
			"RequestSide": RequestSide,
			"ScienceSidebar": ScienceSidebar,
			"Goal": Goal,
			"Shoppingplan": Shoppingplan,
			"Finance": Finance,
			"Security": Security
		};
		var background_image = Icon;
		if(this.props.icon !== "" && this.props.icon !== undefined && components[this.props.icon]!== undefined){
			background_image = components[this.props.icon];
		}
		if(this.props.size !== "" && this.props.size !== undefined ){
			width  = this.props.size;
			height = this.props.size;
		}
		if(this.props.width !== "" && this.props.width !== undefined ){
			width = this.props.width;
		}
		if(this.props.height !== "" && this.props.height !== undefined ){
			height = this.props.height;
		}
		return(
			<div style={{
				backgroundImage: `url(${background_image})`, 
				width:width+"px",
				height:height+"px",
				display:"inline-block",
				backgroundSize: "contain",
				backgroundPosition: "center",
				backgroundRepeat: "no-repeat",
				}}>
			</div>
		)
	}
}
export default svg_01_main_test;