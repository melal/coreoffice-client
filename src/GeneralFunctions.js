import {DataProvider} from './DataProvider';
/**
 * All general functions are in this class;
 */
class GeneralFunctions {
    /**
     * If the token is expired, CheckTokenExpired function get a new token from web service.
     */
    static async CheckTokenExpired() {
        if (new Date(parseInt(sessionStorage.getItem('expires_in'))) - Date.now() < 0) {
            let res = await DataProvider.ConnectToken(undefined,undefined,undefined,undefined,sessionStorage.username,sessionStorage.password);
            if(res.access_token) {
                sessionStorage.setItem('token', res.access_token);
                sessionStorage.setItem('lastUsedTime',Date.now());
                sessionStorage.setItem('expires_length',res.expires_in * 1000);
                let introspectRes = await DataProvider.Introspect();
                sessionStorage.setItem('expires_in', (introspectRes.exp * 1000));
            }
        }
    }
    /**
     * If a user doesn't work with the web application more than expires_length parameter, CheckLastUsed function redirect the uesr to login page;
     */
    static async CheckLastUsed() {
        if (Date.now() - sessionStorage.getItem('lastUsedTime') > sessionStorage.getItem('expires_length')) {
            sessionStorage.clear();
            window.location.replace("/#/Authentication");
        } else {
            sessionStorage.setItem('lastUsedTime', Date.now());
            this.CheckTokenExpired();
        }
    }
    
}

export {
    GeneralFunctions
}