import React from "react";
import Modules from "./config/Module";
import {Switch, Route, HashRouter, withRouter } from "react-router-dom";
import AccessDenied from "./Modules/MainContainer/AccessDenied";

class Routs extends React.Component {
  render() {
    return (
      <HashRouter>
        <Switch>
        {Modules.map((item, index) => 
          <Route exact path={item.Path} component={item.ModuleName} />
        )}
        <Route exact path="*" component={AccessDenied} />
        </Switch>
      </HashRouter>
    );
  }
}

export default withRouter(Routs);
