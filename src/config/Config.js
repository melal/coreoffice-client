const configs = {
    ServerApi: "http://185.22.31.203:5001/api/",
    AuthenticationApi: "http://185.22.31.203:5000/",
    LogLevel: "Warning",
    Version:"1.0.0.1",
    AuthenticationConfig:{
        ClientId:"webclient",
        ClientSecret:"secret",
        GrantType:"password",
        Scope:"api"
    }

}
export default configs;