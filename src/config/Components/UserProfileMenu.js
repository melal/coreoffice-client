import React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';
import Link from '@material-ui/core/Link';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import {changeIsLoggedIn} from '../../actions';
import {connect} from "react-redux";
import configs from '../../config/Config'
//import { AccountCircle } from '@material-ui/icons';
//import { Link as RouterLink } from 'react-router-dom';

function UserProfileMenu(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const updateForm = () => {
    let data = {
        isOnline : false,
        navigatorStringify : ''
    }
    let token = window.localStorage.getItem('token');
    fetch(`http://${configs.AuthenticationApi}/api/users/updateuserinfo`, {
        method: 'PUT', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json',
            'authorization': `Bearer ${token}`,
        },
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(data), // body data type must match "Content-Type" header
    })
    .then(response => response.json())
    .then(data => {
      setTimeout(function(){ console.info('user logged out'); }, 2000);
      window.localStorage.removeItem('token');
       window.location.replace("/");
    });
}

  const logOut = () => {
    
    setAnchorEl(null);
    sessionStorage.clear();
    let token = window.localStorage.getItem('token');
    let formData  = new FormData();
    let data = {
      client_id : 'webclient',
      client_secret : 'secret',
      token : token,
      token_type_hint : 'access_token',
    }

    for(const name in data) {
			formData.append(name, data[name]);
    }
    
    fetch(`${configs.AuthenticationApi}connect/revocation`, {
			method: 'POST', // *GET, POST, PUT, DELETE, etc.
			mode: 'cors', // no-cors, cors, *same-origin
			cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
			credentials: 'same-origin', // include, *same-origin, omit
			headers: {
				// 'Content-Type': 'multipart/form-data; boundary=<calculated when request is sent>',
			},
			// redirect: 'follow', // manual, *follow, error
			referrer: 'no-referrer', // no-referrer, *client
			body: formData, // body data type must match "Content-Type" header
			}).then(result => {
        localStorage.removeItem("token");
        console.info('revoke shod');
        window.location.replace("/#/Authentication");
        props.dispatch(changeIsLoggedIn({
          isLoggedIn: false
        }));
			});
      
  }

  return (
    <React.Fragment>
      <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={'simple-menu'}
              aria-haspopup="true"
              onClick={handleClick}
              color="inherit"
            >
              <PersonPinIcon />
              
            </IconButton>
            <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
              style={{
                direction:'rtl'
              }}
            >
              <MenuItem 
              component={Link}
              href="#/profile"
                style={{
                  fontFamily:'IranSans_Light',
                  fontSize:'13px'
                }}
                onClick={handleClose}>
                  <div>حساب کاربری</div>
                  </MenuItem>
              <Divider />
              <MenuItem 
                style={{
                  fontFamily:'IranSans_Light',
                  fontSize:'13px'
                }}
                onClick={handleClose}>خرید طرح</MenuItem>
              <Divider />
              <MenuItem 
                style={{
                  fontFamily:'IranSans_Light',
                  fontSize:'13px'
                }}
                onClick={logOut} value="kh">خروج</MenuItem>
            </Menu>
    </React.Fragment>
  );
}


export default connect (null)(UserProfileMenu);