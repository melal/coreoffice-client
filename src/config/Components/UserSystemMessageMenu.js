import React, { useState, useEffect } from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import MailIcon from '@material-ui/icons/Mail';
import Badge from '@material-ui/core/Badge';
import Divider from '@material-ui/core/Divider';

export default function UserSystemMessageMenu() {
  let [messages, setMessages] = React.useState([]);

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const goToNotify = (item) => {
    
    setAnchorEl(null);
  }

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <React.Fragment>
      <IconButton
          edge="end"
          aria-label="account of current user"
          aria-controls={'simple-menu'}
          aria-haspopup="true"
          onClick={handleClick}
          color="inherit"
        >
          <Badge 
            badgeContent={messages.length} 
            color="secondary">
            <MailIcon />
          </Badge>
        </IconButton>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        style={{
          direction:'rtl'
        }}
      >
          <div>
            {messages.map(
              (item, index) => 
                <MenuItem 
                style={{
                  fontFamily:'IranSans_Light',
                  fontSize:'13px'
                }}
                onClick={()=>goToNotify(item)}>
                  <div>{item.message}</div>
              </MenuItem>
            )}
            <Divider />
          </div>
      </Menu>
    </React.Fragment>
  );
}