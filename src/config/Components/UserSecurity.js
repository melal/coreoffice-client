import React from 'react';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import { Typography } from '@material-ui/core';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

class UserSecurity extends React.Component{
	
	constructor(props) {
		super (props);
		this.state = {
			newPassword : null,
			newConfirmPassword : null,
			oldPassword : null,
			open: false
		}
	}
	
	componentDidMount(props) {

	}

	setOldPassword = (e) => {
		this.setState({
			oldPassword : e.target.value
		});
	}

	setNewPassword = (e) => {
		this.setState({
			newPassword : e.target.value
		});
	}
	
	setNewConfirmPassword = (e) => {
		this.setState({
			newConfirmPassword : e.target.value
		});
	}

	updateNewPassword = () => {
		var token = window.localStorage.getItem('token');
		let newUserData = {
			newPassword:this.state.newPassword,
			oldPassword:this.state.oldPassword
		}
		fetch(process.env.REACT_APP_API_URL + `users/updatepassword`, {
		method: 'POST', // *GET, POST, PUT, DELETE, etc.
		mode: 'cors', // no-cors, cors, *same-origin
		cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
		credentials: 'same-origin', // include, *same-origin, omit
		headers: {
		'Content-Type': 'application/json',
		'authorization': `Bearer ${token}`,
		},
		redirect: 'follow', // manual, *follow, error
		referrer: 'no-referrer', // no-referrer, *client
		body: JSON.stringify(newUserData), // body data type must match "Content-Type" header
		})
		.then(response => response.json())
		.then(result => {
			window.localStorage.setItem('token', result.token);
			console.info('password changed');
			this.setState({
				open: true
			});
		});
	}
	handleClose = () => {
		this.setState({
			open: false
		});
	}
	render() {
		return (
			<React.Fragment>
				<Snackbar 
					open={this.state.open} 
					autoHideDuration={2000}
					onClose={this.handleClose}
					>
					<Alert 
						// onClose={handleClose} 
						severity="success"
						>
						<Typography
						style={{
							fontFamily:'IranSans'
						}}
						>
						کلمه عبور شما با موفقیت به روزرسانی شد
						</Typography>
					
					</Alert>
				</Snackbar>
				<Grid container spacing={1} justify="center"
				 style={{
					textAlign:"right",
					direction:"rtl",
					maxWidth:"100%"
				}}>
					<Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
						<TextField
							fullWidth
							id="standard-name"
							label="کلمه عبور فعلی"
							value={this.state.oldPassword}
							onChange={this.setOldPassword}
							margin="normal"
							InputLabelProps={{
								style: {
									fontFamily: "IranSans"
								}
							}}
							InputProps={{
								style: {
									fontFamily: "IranSans"
								}
							}}
						/>
						<TextField
							fullWidth
							type="password"
							id="standard-name"
							label="کلمه عبور جدید"
							value={this.state.newPassword}
							onChange={this.setNewPassword}
							margin="normal"
							InputLabelProps={{
								style: {
									fontFamily: "IranSans"
								}
							}}
							InputProps={{
								style: {
									fontFamily: "IranSans"
								}
							}}
						/>
						<TextField
							fullWidth
							type="password"
							id="standard-name"
							label="تایید کلمه عبور جدید"
							value={this.state.newConfirmPassword}
							onChange={this.setNewConfirmPassword}
							margin="normal"
							InputLabelProps={{
								style: {
									fontFamily: "IranSans"
								}
							}}
							InputProps={{
								style: {
									fontFamily: "IranSans"
								}
							}}
						/>
					   

					</Grid>
					<Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
						<Button
							type="submit"
							variant="contained"
							component="button"
							style={{ fontFamily: 
									'IranSans', 
									display:"flex",
									margin:"10px auto",
									backgroundColor:'#0c7f99',
									color: 'white'
								}}
							disabled={(this.state.newPassword !== this.state.newConfirmPassword) ? true : false}
							onClick ={this.updateNewPassword}
						>
						به روز رسانی کلمه عبور
						</Button>
					</Grid>
					<Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
						<Paper>
							<Typography
							style={{
								fontFamily:'IranSans'
							}}
							>
								در حال حاضر، حساب کاربری شما در دستگاه‌های زیر فعال است.
							</Typography>
							<TableContainer component={Paper}>
								<Table>
									<TableHead>
									<TableRow>
										<TableCell>نام دستگاه</TableCell>
									</TableRow>
									</TableHead>
									<TableBody>
										<TableRow>
										<TableCell component="th" scope="row">
											{window.navigator.appVersion}
										</TableCell>
										</TableRow>
									</TableBody>
								</Table>
							</TableContainer>
						</Paper>
					</Grid>
				</Grid>
				
				
			</React.Fragment>
		)
	}
}
export default UserSecurity;