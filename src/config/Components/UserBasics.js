import React from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import { Typography } from '@material-ui/core';
import { createMuiTheme } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';

class UserBasics extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			firstName : '',
			lastName: '',
			email:'',
			mobile:'',
			grade :''
		};
	}

	componentDidMount() {
		console.info('user basics');
		let token = window.localStorage.getItem('token');
	}

	handleChange = pr => event => {
		event.persist();
		this.setState({[pr]: event.target.value});
	};

	updateForm = () => {
		let data = {
			firstName : this.state.firstName,
			lastName : this.state.lastName,
			mobile : this.state.mobile,
			email : this.state.email,
			grade : this.state.grade
		}
		let token = window.localStorage.getItem('token');
		fetch(process.env.REACT_APP_API_URL+`users/updateuserinfo`, {
			method: 'PUT', // *GET, POST, PUT, DELETE, etc.
			mode: 'cors', // no-cors, cors, *same-origin
			cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
			credentials: 'same-origin', // include, *same-origin, omit
			headers: {
				'Content-Type': 'application/json',
				'authorization': `Bearer ${token}`,
			},
			redirect: 'follow', // manual, *follow, error
			referrer: 'no-referrer', // no-referrer, *client
			body: JSON.stringify(data), // body data type must match "Content-Type" header
		})
		.then(response => response.json())
		.then(data => {
			this.setState((state, props) => {
				return {
					firstName: data.data.firstName,
					lastName: data.data.lastName,
					email: data.data.email,
					mobile: data.data.mobile
				}
			}, () => {
				window.location.reload();
			});
		});
	}
	
	render() {
		const theme = createMuiTheme();
		const classes = {
			Text_style:{
				fontFamily: "IranSans",
				textAlign:"center"
			}
		}
		return(
			<React.Fragment>
				<Grid container justify="center" style={{
					textAlign:"right",
					direction:"rtl",
					maxWidth:"100%"
				}}> 
					<Grid item xs={12} sm={12} md={12} lg={12} xl={12}  justify="center"   alignItems="center">
						
					</Grid>
					<Grid item xs={12} sm={12} md={12} lg={12} xl={12}  justify="center"   alignItems="center">
						<Button
							type="submit"
							variant="contained"
							component="button"
							style={{ 
								fontFamily: 'IranSans', 
								display:"flex" ,
								margin: "0 auto",
								backgroundColor :'#02A194',
								color: 'white'
							}}
						>
							ثبت تصویر جدید
						</Button> 
					</Grid>
				</Grid>
				<Grid container spacing={1} justify="center"> 
					<Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
						<TextField
							fullWidth
							id="standard-name"
							label="نام"
							value={this.state.firstName}
							onChange={this.handleChange('firstName')}
							margin="normal"
							InputLabelProps={{
								style: {
									fontFamily: "IranSans",
								}
							}}
							InputProps={{
								style: {
									fontFamily: "IranSans",
									textAlign:"center"
								}
							}}
						/>
					</Grid>
					<Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
						<TextField
						fullWidth
						id="standard-name"
						label="نام خانوادگی"
						value={this.state.lastName}
						onChange={this.handleChange('lastName')}
						margin="normal"
						InputLabelProps={{
							style: {
								fontFamily: "IranSans"
							}
						}}
						InputProps={{
							style: {
								fontFamily: "IranSans",
								textAlign:"center"
							}
						}}
						/>
					</Grid>
					<Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
						<TextField
						fullWidth
						id="standard-name"
						label="ایمیل"
						InputLabelProps={{
							style: {
								fontFamily: "IranSans"
							}
						}}
						InputProps={{
							style: {
								fontFamily: "IranSans",
								textAlign:"center"
							}
						}}
						value={this.state.email}
						onChange={this.handleChange('email')}
						margin="normal"
						/>
					</Grid>
					<Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
						<TextField
						fullWidth
						disabled
						id="standard-name"
						label="موبایل"
						InputLabelProps={{
							style: {
								fontFamily: "IranSans"
							}
						}}
						value={this.state.mobile}
						onChange={this.handleChange('mobile')}
						margin="normal"
						/>
					</Grid>
					<Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
					<FormControl 
						style={{ 
							direction: 'rtl',
							fontFamily: "IranSans",
							display: 'block'
							}}>
						<Typography
						style={{
							fontFamily:'IranSans',
							fontSize:'14px',
							direction: 'rtl',
							color:'rgba(0, 0, 0, 0.54)'
						}}
						>پایه تحصیلی</Typography>
						<Select
							MenuProps={{ style: { direction: 'rtl', fontFamily:'IranSans' } }}
							fullWidth
							value={this.state.grade}
							onChange={this.handleChange('grade')}
							inputProps={{
								name: 'age',
								id: 'age-simple',
							}}
							>
							<MenuItem value={10}><Typography style={{fontFamily:'IranSans', fontSize:'12px'}}>دهم</Typography></MenuItem>
							<MenuItem value={20}><Typography style={{fontFamily:'IranSans', fontSize:'12px'}}>یازدهم</Typography></MenuItem>
							<MenuItem value={30}><Typography style={{fontFamily:'IranSans', fontSize:'12px'}}>دوازدهم</Typography></MenuItem>
							<MenuItem value={40}><Typography style={{fontFamily:'IranSans', fontSize:'12px'}}>فارغ التحصیل</Typography></MenuItem>
						</Select>
						</FormControl>
					</Grid>
					<Grid item xs={12} sm={12} md={12} lg={12} xl={12}  justify="center"   alignItems="center">
						<Button
						type="submit"
						variant="contained"
						component="button"
						style={{ fontFamily: 'IranSans', margin:"0 auto", display: "flex", backgroundColor :'#02A194',
						color: 'white' }}
						onClick={this.updateForm}
						>
							آپدیت اطلاعات
						</Button>
					</Grid>
				</Grid>
			</React.Fragment>
		);
	}
}

export default UserBasics;