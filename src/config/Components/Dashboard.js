import React from "react";
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

class Dashboard extends React.Component {
    
    constructor(props) {
        super (props);
        this.state = {
            userinfo : {},
            myPlan : []
        }
    }

    componentDidMount() {
        console.info('hi there body');
        let token = window.localStorage.getItem('token');
    }

    sendInvitation = (e) => {
        console.info('send invitation');
    }

    returnCorrectMoneyFormat = (money) => {
        return new Intl.NumberFormat('fa-IR').format(money);
	}

    setEmailForInvite = (e) => {
        this.setState({
            emailForInvite : e.target.value
        });
    }

    render() {
        return(
            
        <React.Fragment>
            <Grid container spacing={1} alignItems='center' alignContent='center'>
            <Grid item xs={4} sm={4} md={4} lg={4} xl={4} >
                    <img 
                        style={{
                            display:'block',
                            width: '50px',
                            margin: '9px',
                            borderRadius: '8px',
                            margin: 'auto'
                        }}
                        src="assets/images/credit.png" 
                        />
                    <Typography
                        className="invitehelp"
                        style={{
                            fontFamily:'IranSans',
                            textAlign:'center'
                        }}
                        >
                        تعداد روز مرخصی مانده
                    </Typography>
                    <Typography
                        className="invitehelp"
                        style={{
                            fontFamily:'IranSans',
                            fontSize: '12px',
                            textAlign:'center',
                            direction: 'rtl'
                        }}
                        >
                            {/* {this.state.myPlan.jalaaliEndDate} */}
                            {this.returnCorrectMoneyFormat(135)}
                        {/* // {(this.state.myPlan.status !== 'expire') ? this.state.myPlan.shoppingPlanName : 'بدون اشتراک'} */}
                    </Typography>
                {/* </Paper> */}
            </Grid>
            <Grid item xs={4} sm={4} md={4} lg={4} xl={4} >
                {/* <Paper
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        flexDirection: 'column'
                    }}
                    > */}
                    <img 
                        style={{
                            display:'block',
                            width: '50px',
                            margin: '9px',
                            borderRadius: '8px',
                            margin: 'auto'
                        }}
                        src="assets/images/fpoint.png" 
                        />
                    <Typography
                        className="invitehelp"
                        style={{
                            fontFamily:'IranSans',
                            textAlign:'center'
                        }}
                        >
                        میزان حقوق
                    </Typography>
                    <Typography
                        className="invitehelp"
                        style={{
                            fontFamily:'IranSans',
                            fontSize: '12px',
                            textAlign:'center'
                        }}
                        >
                        {/* {this.returnCorrectMoneyFormat(this.state.userinfo.fCoin)} */}
                        {this.returnCorrectMoneyFormat(2000000)} تومان
                    </Typography>
                {/* </Paper> */}
            </Grid>
            <Grid item xs={4} sm={4} md={4} lg={4} xl={4} >
                {/* <Paper
                    style={{
                        display: 'flex',
                        alignItems: 'center',
                        flexDirection: 'column'
                    }}
                    > */}
                    <img 
                        style={{
                            display:'block',
                            width: '50px',
                            margin: '9px',
                            borderRadius: '8px',
                            margin: 'auto'
                        }}
                        src="assets/images/icon-128x128.png" 
                        />
                    <Typography
                        className="invitehelp"
                        style={{
                            fontFamily:'IranSans',
                            textAlign:'center'
                        }}
                        >
                        تعداد احکام
                    </Typography>
                    <Typography
                        className="invitehelp"
                        style={{
                            fontFamily:'IranSans',
                            fontSize: '12px',
                            textAlign:'center',
                            direction : 'rtl'
                        }}
                        >
                        {/* {this.returnCorrectMoneyFormat(this.state.userinfo.invitationNewUserNumber)}  */}
                        {this.returnCorrectMoneyFormat(25)} 
                    </Typography>
                {/* </Paper> */}
            </Grid>
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12} xl={12} >
            </Grid>
           
            <Grid item xs={12} sm={12} md={12} lg={12} xl={12} >
            </Grid>
        </React.Fragment>
        );
    }
}

export default Dashboard;