import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Pagination from '@material-ui/lab/Pagination';

class Inbox extends React.Component {
  
	constructor (props) {
	super (props);
	this.state = {
	messages : []
	}
	}
	makeMessagesIsRead = () => {
		let token = localStorage.getItem('token');
		
	}

	fetchMessages = () => {
		let token = localStorage.getItem('token');
		
	}

	componentDidMount() {
			this.fetchMessages();
			this.makeMessagesIsRead();
	}
	
	returnCorrectMoneyFormat = (money) => {
        return new Intl.NumberFormat('fa-IR').format(money);
    }
	
	handleChange = (event, value) => {
		console.info(parseInt(event.target.innerHTML[0]));
	}

	render() {
		return (
			<Paper
			style={{
				padding : '5px'
			}}
			>
				<Typography
				style={{
					fontFamily : 'IranSans',
					direction : 'rtl'
				}}
				>
					شما {this.returnCorrectMoneyFormat(this.state.messages.length)} پیام دارید
				</Typography>
				<List
				
				>
					{this.state.messages.map(
						(message, index) => 
						<ListItem 
						alignItems="flex-start"
						key = {index}
						style={{
							backgroundColor : '#eae8e8',
							borderRadius : '3px',
							padding : '5px',
							marginBottom: '4px'
						}}
						>
						
							<ListItemText
							primary={<Typography style={{fontFamily: 'IranSans', fontSize:'13px'}}>{message.message}</Typography>}
							secondary={
								<React.Fragment>
								
								<Typography
								style={{fontFamily: 'IranSans', fontSize:'13px'}}
								>{message.jalaaliFullUserFriendlyCreatedDate}</Typography>
								</React.Fragment>
							}
							style={{textAlign:"right", fontFamily: "IranSans"}}
							/>
						</ListItem>
					)}
				</List>
				<div style={{
					textAlign: 'center'
				}}>
					<Pagination 
						style={{
							display: 'inline-block'
						}}
						count={5}
						onChange={(event) => this.handleChange(event)}
						/>
				</div>
			</Paper>
		)
	};
}
export default Inbox;