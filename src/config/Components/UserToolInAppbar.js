import React from 'react';
import UserProfileMenu from '../Components/UserProfileMenu';
import UserSystemMessageMenu from '../Components/UserSystemMessageMenu';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import {connect} from "react-redux";
import '../../content/css/MainContainer/Authentication/authentication.css'

//import UserNotificationMenu from './UserNotificationMenu';


class UserToolInAppbar extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            userinfo : null
        }
    }

    componentDidMount(){
        
    }

    render() {
        let token = window.sessionStorage.getItem('token');
        if (this.props.token || token)
            return(
                <React.Fragment>
                    <Typography 
                        color="textPrimary"
                        style={{
                            color:'inherit',
                            fontFamily:'IranSans_Light',
                            fontSize:'12px',
                            textAlign:'rigth'
                        }}
                        >
                        {/* {this.state.userinfo.firstName + ' ' + this.state.userinfo.lastName}    */}
                        میلاد خان محمدی
                    </Typography>
                    
                    <UserSystemMessageMenu />
                    {/* <UserNotificationMenu /> */}
                    <UserProfileMenu 
                        // userinfo = {this.state.userinfo}
                    />
                </React.Fragment>
            )
        else 
            return (
                <React.Fragment>
                    <Button 
                        variant="outlined"
                        href="#/Authentication"
                        className="btnLogin"
                        >
                        ورورد به سیستم
                    </Button>
                </React.Fragment>
            )
    }
}

const mapStateToProps = state => {
    const token = state.token;
    return {
	    token
	}
}

export default connect(mapStateToProps)(UserToolInAppbar);