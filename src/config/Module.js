import Home from "../Modules/Home/Home";
import Authentication from "../Modules/MainContainer/Authentication";
import ForgetPassword from "../config/Components/ForgetPassword";
import Profile from "../Modules/Profile/Profile";
import AccessDenied from "../Modules/MainContainer/AccessDenied";
import HR from "../Modules/HR/HR";

const Modules = [
  {
    Path: "/Home",
    ModuleName: Home,
  },
  {
    Path: "/Authentication",
    ModuleName: Authentication,
  },
  {
    Path: "/hr",
    ModuleName: HR,
  },
];

export default Modules;
