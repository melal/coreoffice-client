import React from 'react';
import Routes from './Routes.js';
import './App.css';
import store from './store';
import {GeneralFunctions} from './GeneralFunctions';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { createStore } from 'redux';
import { Provider } from 'react-redux';
import MainContainer from "./maincontainer";
import DataProvider from "./DataProvider";


class App extends React.PureComponent {
  state = {

  }


  render(){
    return (
      <Provider store={store}>
        <MainContainer />
      </Provider>
    );
  }
}

document.body.addEventListener("click",()=>{
  GeneralFunctions.CheckLastUsed();
})

export default App;