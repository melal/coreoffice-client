const INCREMENT_COUNTER = 'INCREMENT_COUNTER';
const DECREMENT_COUNTER = 'DECREMENT_COUNTER';
const ADD_TOKEN_TO_STATE = 'ADD_TOKEN_TO_STATE';
const ADD_TO_CUSTOM_SIDEMENU = 'ADD_TO_CUSTOM_SIDEMENU';
const REMOVE_FROM_CUSTOM_SIDEMENU = 'REMOVE_FROM_CUSTOM_SIDEMENU';
const ADD_SELECTED_FEATURE_TO_STATE = 'ADD_SELECTED_FEATURE_TO_STATE';
const ADD_FLAT_LIST_TO_STATE = 'ADD_FLAT_LIST_TO_STATE';
const CHANGE_IS_LOGGED_IN = 'CHANGE_IS_LOGGED_IN';
const POPULATEMENU = 'POPULATEMENU';

const increment = () => ({type:INCREMENT_COUNTER});
const decrement = () => ({type:DECREMENT_COUNTER});

const addToCustomSideMenu = (data) => (
  {
    type:ADD_TO_CUSTOM_SIDEMENU,
    data :data
  }
  );

const populateMenu = (data) => (
  {
    type:POPULATEMENU,
    data :data
  }
  );



const addFlatlistToState = (data) => (
  {
    type:ADD_FLAT_LIST_TO_STATE,
    data :data
  }
  );

const addSelectedFeaturetoState = (data) => {
  console.info('test:', data);
  return {
    type:ADD_SELECTED_FEATURE_TO_STATE,
    data :data
  }
}

const removeFromCustomSideMenu = () => ({type:REMOVE_FROM_CUSTOM_SIDEMENU});
const addTokenToState = (token) => (
  {
    type:ADD_TOKEN_TO_STATE,
    token : token
  }
  );

  const changeIsLoggedIn = (isLoggedIn)=>({
    type:CHANGE_IS_LOGGED_IN,
    data : isLoggedIn
  })

export {
  ADD_TOKEN_TO_STATE,
  INCREMENT_COUNTER,
  DECREMENT_COUNTER,
  ADD_TO_CUSTOM_SIDEMENU,
  REMOVE_FROM_CUSTOM_SIDEMENU,
  ADD_SELECTED_FEATURE_TO_STATE,
  ADD_FLAT_LIST_TO_STATE,
  CHANGE_IS_LOGGED_IN,
  POPULATEMENU,
  addFlatlistToState,
  increment,
  decrement,
  addToCustomSideMenu,
  removeFromCustomSideMenu,
  addTokenToState,
  addSelectedFeaturetoState,
  changeIsLoggedIn,
  populateMenu
}