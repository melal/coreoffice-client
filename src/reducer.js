import {
  ADD_TOKEN_TO_STATE,
  INCREMENT_COUNTER, 
  DECREMENT_COUNTER, 
  ADD_TO_CUSTOM_SIDEMENU, 
  REMOVE_FROM_CUSTOM_SIDEMENU,
  ADD_SELECTED_FEATURE_TO_STATE,
  ADD_FLAT_LIST_TO_STATE,
  CHANGE_IS_LOGGED_IN,
  POPULATEMENU
} from "./actions";
const initialState = {
  userInfo : {
    firstName : 'میلاد',
    lastName : 'خانمحمدی',
    thumbnail : '',
    token : null,
    featuresFlatList : []
  },
  selectedFeature : {
    name : ''
  },
  custom_side_menu : [
    {
      // 'icon':<Icons/>, 
      'text':'صفحه اصلی',
      'link':'/'
    }
  ],
  isLoggedIn: false,
  menu: []
}
const reducer = (state = initialState, action) => {
  switch (action.type) {

    case INCREMENT_COUNTER:
      return {
        ...state,
        counter:state.counter + 1
      };

    case POPULATEMENU:
      return {
        ...state,
        menu :action.data.menu
      };

    case CHANGE_IS_LOGGED_IN:
      return {
        ...state,
        isLoggedIn: action.data.isLoggedIn
      };
    
    case ADD_TOKEN_TO_STATE:
      return {
        ...state,
        token: action.token.token
      }

    case ADD_FLAT_LIST_TO_STATE:
      return {
        ...state,
        featuresFlatList: action.data.data
      };
    
    case ADD_SELECTED_FEATURE_TO_STATE:
      return {
        ...state,
        selectedFeature: action.data.selectedobject
      };
    
    case DECREMENT_COUNTER:
      return {
        ...state,
        counter:state.counter - 1
      };
    
    case ADD_TO_CUSTOM_SIDEMENU:
      let custom_side_menu_temp = state.custom_side_menu;
      custom_side_menu_temp.push(action.data)
      return {
        ...state,
        custom_side_menu:custom_side_menu_temp
      };
    
    case REMOVE_FROM_CUSTOM_SIDEMENU:
      return {
        ...state,
        counter:state.counter - 1
      };
    
    default:
      return state;

  }
}

export default reducer;