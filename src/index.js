import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import { HashRouter } from "react-router-dom";
import { SnackbarProvider } from 'notistack';
import 'font-awesome/css/font-awesome.css';

ReactDOM.render(
  <HashRouter>
    <SnackbarProvider maxSnack={3}>
      <App />
    </SnackbarProvider>
  </HashRouter>
  ,
  document.getElementById('root')
);

serviceWorker.unregister();
