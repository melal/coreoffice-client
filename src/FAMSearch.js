import React from 'react';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import './search.css';

class FAMSearch extends React.PureComponent {
    
    constructor(props){
        super(props);
    }
    
    state = {
        parameters: ['Persons', 'Documents','Features','Types'],
        showParamtersList : false,
        showSelectedParameter: false,
        searchValue :''
    }

    componentDidMount() {

    }
    
    selectParamter = (e, item) => {
        this.setState({
            selectedParameter : [item],
            showSelectedParameter : true,
            showParamtersList : false,
            searchValue : ''
        }, ()=>{
            console.info(this.state.selectedParameter[0]);
        })
    }
    
    checkInput = (e) => {
        if(e.target.value[0] === '@') {
            this.setState({
                showParamtersList : true,
                searchValue : e.target.value
            });
        } else {
            this.setState({
                searchValue : e.target.value
            });
        }
    }

    showKeyCode = (event) => {
        switch(event.keyCode) {
            case 8:
                if(this.state.searchValue.length == 0)
                    this.setState({
                        showSelectedParameter : false
                    });
                break;
        }
    }

    render() {
        return(
            <div 
                // className={classes.search} 
                edge="start"
                style={{
                    // backgroundColor: 'white',
                    // border: '1px solid #cbcbcb'
                }}
                >
                <div 
                    // className={classes.searchIcon}
                    >
                    <SearchIcon />
                </div>
                
                <div 
                    className={!this.state.showSelectedParameter ? "fam-search-selected-parameter": "fam-search-selected-parameter show-selected-parameter"}
                >@{this.state.selectedParameter ? this.state.selectedParameter[0] : null}:</div>
                
                <InputBase
                    onChange={this.checkInput}
                    onKeyDown={this.showKeyCode}
                    placeholder="جستجو کنید"
                    inputProps={{
                        'aria-label': 'search',
                        style: {
                            fontFamily: "IranSans_Light",
                            fontSize: '13px'
                        }
                    }}
                    value = {this.state.searchValue}
                />
                <div 
                    className = {!this.state.showParamtersList? "fam-search-parameters-container" : "fam-search-parameters-container show"}
                    >
                        {this.state.parameters.map(
                            (item, index) =>
                                <div
                                    onClick={(e) => this.selectParamter(e,item)}
                                    >{item}</div>
                            )
                        }
                </div>
                
                <div
                    className="fam-search-container"
                    >
                        {/* Persons */}
                <div>
                    <div
                        style={{
                            textAlign: 'right',
                            backgroundColor: '#ececec',
                            color: '#777777',
                            fontFamily: 'Almarai',
                            fontSize: '11px',
                            lineHeight: '28px',
                            paddingRight: '1%',
                        }}
                        >اشخاص
                    </div>
                        {[1,2,3].map(
                            (item, index) => 
                            <div
                            className = "fam-search-user-row"
                            >
                            <img
                                src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/6233935/avatar.png?width=40" />
                            <div
                            style={{
                                marginRight: '8px'
                            }}
                            >
                                <div
                                    style={{
                                        fontFamily: 'Almarai',
                                        fontSize: '12px'
                                    }}
                                    >
                                    محمد شهابی پور
                                </div>
                                <div
                                    style={{
                                        fontFamily: 'Almarai',
                                        textAlign: 'right',
                                        color: '#878787',
                                        fontSize: '12px',
                                    }}
                                    >
                                    کارمند
                                </div>
                            </div>
                        </div>
                        )}
                        <div
                            style={{
                                textAlign: 'right',
                                backgroundColor: '#ececec',
                                color: '#777777',
                                fontFamily: 'Almarai',
                                fontSize: '11px',
                                lineHeight: '28px',
                                paddingRight: '1%',
                            }}
                            >اسناد
                        </div>
                        {[1,2,3].map(
                            (item, index) => 
                            <div
                            className = "fam-search-user-row"
                            >
                            <img
                                src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/6233935/avatar.png?width=40" />
                            <div
                            style={{
                                marginRight: '8px'
                            }}
                            >
                                <div
                                    style={{
                                        fontFamily: 'Almarai',
                                        fontSize: '12px'
                                    }}
                                    >
                                    محمد شهابی پور
                                </div>
                                <div
                                    style={{
                                        fontFamily: 'Almarai',
                                        textAlign: 'right',
                                        color: '#878787',
                                        fontSize: '12px',
                                    }}
                                    >
                                    کارمند
                                </div>
                            </div>
                        </div>
                        )}
                    </div>
                    <div
                    style={{
                        backgroundColor: '#00BCD4',
                        fontFamily: 'Almarai',
                        color: 'white',
                        fontSize: '11px',
                        padding: '5px 16px',
                        marginTop: '6px',
                    }}
                    >نمایش همه اطلاعات یافت شده</div>
                </div>    
            </div>
        );
    }
}
export default FAMSearch;