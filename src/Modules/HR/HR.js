import React, { Fragment, useEffect, useState } from 'react';
import {AppBar,Select, MenuItem, Typography  ,Grid, IconButton, Tooltip,InputBase,Table,TableBody,TableCell,TableContainer,TableHead,TableRow,Paper,TableSortLabel,Checkbox,TablePagination,Toolbar,FormControlLabel,Switch,Button, TextField, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle    } from '@material-ui/core';
import {AddBox,Edit,Delete,Search} from '@material-ui/icons';
import { lighten, makeStyles, fade   } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import {DataProvider} from '../../DataProvider';
import DatePicker from 'react-datepicker2';
import momentjalaali from 'moment-jalaali';
import PersonIcon from '@material-ui/icons/Person';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import ProfileDialogPerson from './ProfileDialogPerson';

import { SnackbarProvider, useSnackbar } from 'notistack';

export default function HR(){
            const {enqueueSnackbar} = useSnackbar();
            const [order, setOrder] = React.useState('asc');
            const [orderBy, setOrderBy] = React.useState('calories');
            const [selected, setSelected] = React.useState([]);
            const [page, setPage] = React.useState(0);
            const [dense, setDense] = React.useState(false);
            const [rowsPerPage, setRowsPerPage] = React.useState(5);
            const [addDialog, setAddDialog] = React.useState(false);
            const [editDialog, setEditDialog] = React.useState(false);
            const [rows, setRowsData] = React.useState([]);
            const [firstName, setFirstName] = React.useState('');
            const [lastName, setLastName] = React.useState('');
            const [gender, setGender] = React.useState(1);
            const [fatherName, setFatherName] = React.useState('');
            const [birthDate, setBirthDate] = React.useState(undefined);
            const [birthPlace, setBirthPlace] = React.useState('');
            const [startDate, setStartDate] = React.useState(undefined);
            const [endDate, setEndDate] = React.useState(undefined);
            const [firstNameError, setFirstNameError] = React.useState(false);
            const [firstNameErrorMessage, setFirstNameErrorMessage] = React.useState('');
            const [lastNameError, setLastNameError] = React.useState(false);
            const [lastNameErrorMessage, setLastNameErrorMessage] = React.useState('');
            const [fatherError, setFatherError] = React.useState(false);
            const [fatherErrorMessage, setFatherErrorMessage] = React.useState('');
            const [birthPlaceError, setBirthPlaceError] = React.useState(false);
            const [birthPlaceErrorMessage, setBirthPlaceErrorMessage] = React.useState('');
            const [openProfileDialog, setOpenProfileDialog] = React.useState(false);
            const [tabPanelValue, setTabPanelValeu] = React.useState(0);
            const [currentUser, setCurrentUser] = React.useState([]);
            useEffect(()=>{
                async function fetchData(){
                    let headers = {
                        'authorization' : `Bearer ${sessionStorage.getItem('token')}`
                    };
                    let params = {
                        orderby : 'id',
                        ordertype : 'asc',
                        offset : 0,
                        limit: 100
                    }
                    const res = await DataProvider.GET('persons/get',headers,params);
                    if(typeof res === 'undefined') return;
                    let tempArr = [];
                    for(let elm of res.data){
                        tempArr.push({'id': elm.id,'firstName': elm.firstName, 'lastName': elm.lastName, 'gender': elm.gender ,'fatherName': elm.fatherName, 'birthDate': elm.birthDate, 'birthPlace': elm.birthPlace, 'startDate': elm.startDate, 'endDate': elm.endDate});
                    }
                    setRowsData(tempArr);
                }
                fetchData();
            },[])

           
            
            const [headCells, setHeadCells] = React.useState([
                { id: 'id', numeric: false, disablePadding: true, label: 'ردیف' },
                { id: 'firstName', numeric: false, disablePadding: true, label: 'نام' },
                { id: 'lastName', numeric: false, disablePadding: false, label: 'نام خانوادگی' },
                { id: 'gender', numeric: false, disablePadding: false, label: 'جنسیت' },
                { id: 'fatherName', numeric: true, disablePadding: false, label: 'نام پدر' },
                { id: 'birthDate', numeric: true, disablePadding: false, label: 'تاریخ تولد' },
                { id: 'birthPlace', numeric: true, disablePadding: false, label: 'محل تولد' },
                { id: 'startDate', numeric: true, disablePadding: false, label: 'تاریخ شروع' },
                { id: 'endDate', numeric: true, disablePadding: false, label: 'تاریخ پایان' },
                { id: 'operation', numeric: true, disablePadding: false, label: 'عملیات' },
            ]);
            
            const handleOperationButtons = (events,rowid)=>{
                let currentUser = rows.find((value,index)=>{return(value.id === rowid)});
                setCurrentUser(currentUser);
                setOpenProfileDialog(true);
            }

            const handleCloseProfileDialog = () => {
                console.log(openProfileDialog);
                setOpenProfileDialog(false);
            }
            
            function descendingComparator(a, b, orderBy) {
            if (b[orderBy] < a[orderBy]) {
                return -1;
            }
            if (b[orderBy] > a[orderBy]) {
                return 1;
            }
            return 0;
            }
            
            function getComparator(order, orderBy) {
            return order === 'desc'
                ? (a, b) => descendingComparator(a, b, orderBy)
                : (a, b) => -descendingComparator(a, b, orderBy);
            }
            
            function stableSort(array, comparator) {
            const stabilizedThis = array.map((el, index) => [el, index]);
            stabilizedThis.sort((a, b) => {
                const order = comparator(a[0], b[0]);
                if (order !== 0) return order;
                return a[1] - b[1];
            });
            return stabilizedThis.map((el) => el[0]);
            }
            
            
            
            function EnhancedTableHead(props) {
            const { classes, onSelectAllClick, order, orderBy, numSelected, rowCount, onRequestSort } = props;
            const createSortHandler = (property) => (event) => {
                onRequestSort(event, property);
            };
            
            return (
                <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">
                    <Checkbox
                        indeterminate={numSelected > 0 && numSelected < rowCount}
                        checked={rowCount > 0 && numSelected === rowCount}
                        onChange={onSelectAllClick}
                        inputProps={{ 'aria-label': 'select all desserts' }}
                    />
                    </TableCell>
                    {headCells.map((headCell) => (
                    <TableCell
                        key={headCell.id}
                        align={headCell.numeric ? 'right' : 'left'}
                        padding={headCell.disablePadding ? 'none' : 'default'}
                        sortDirection={orderBy === headCell.id ? order : false}
                    >
                        <TableSortLabel
                        active={orderBy === headCell.id}
                        direction={orderBy === headCell.id ? order : 'asc'}
                        onClick={createSortHandler(headCell.id)}
                        >
                        {headCell.label}
                        {orderBy === headCell.id ? (
                            <span className={classes.visuallyHidden}>
                            {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                            </span>
                        ) : null}
                        </TableSortLabel>
                    </TableCell>
                    ))}
                </TableRow>
                </TableHead>
            );
            }
            
            EnhancedTableHead.propTypes = {
            classes: PropTypes.object.isRequired,
            numSelected: PropTypes.number.isRequired,
            onRequestSort: PropTypes.func.isRequired,
            onSelectAllClick: PropTypes.func.isRequired,
            order: PropTypes.oneOf(['asc', 'desc']).isRequired,
            orderBy: PropTypes.string.isRequired,
            rowCount: PropTypes.number.isRequired,
            };
            
            const useToolbarStyles = makeStyles((theme) => ({
            root: {
                paddingLeft: theme.spacing(2),
                paddingRight: theme.spacing(1),
            },
            highlight:
                theme.palette.type === 'light'
                ? {
                    color: theme.palette.secondary.main,
                    backgroundColor: lighten(theme.palette.secondary.light, 0.85),
                    }
                : {
                    color: theme.palette.text.primary,
                    backgroundColor: theme.palette.secondary.dark,
                    },
            title: {
                flex: '1 1 100%',
            },
            }));
            
            const EnhancedTableToolbar = (props) => {
            const classes = useToolbarStyles();
            const { numSelected } = props;
            
            return (
                <Toolbar
                className={clsx(classes.root, {
                    [classes.highlight]: numSelected > 0,
                })}
                >
                {numSelected > 0 ? (
                    <Typography className={classes.title} color="inherit" variant="subtitle1" component="div">
                    {numSelected} selected
                    </Typography>
                ) : (
                    <Typography className={classes.title} variant="h6" id="tableTitle" component="div">
                    Nutrition
                    </Typography>
                )}
            
                {numSelected > 0 ? (
                    <Tooltip title="Delete">
                    <IconButton aria-label="delete">
                        <DeleteIcon />
                    </IconButton>
                    </Tooltip>
                ) : (
                    <Tooltip title="Filter list">
                    <IconButton aria-label="filter list">
                        <FilterListIcon />
                    </IconButton>
                    </Tooltip>
                )}
                </Toolbar>
            );
            };
            
            EnhancedTableToolbar.propTypes = {
            numSelected: PropTypes.number.isRequired,
            };
            
            const useStyles = makeStyles((theme) => ({
            root: {
                width: '100%',
            },
            p100: {
                paddingTop: '100px',
            },
            
            paper: {
                width: '100%',
                marginBottom: theme.spacing(2),
            },
            table: {
                minWidth: 750,
                direction: 'rtl',
            },
            dialogRtl:{
                direction: 'rtl',
            },
            visuallyHidden: {
                border: 0,
                clip: 'rect(0 0 0 0)',
                height: 1,
                margin: -1,
                overflow: 'hidden',
                padding: 0,
                position: 'absolute',
                top: 20,
                width: 1,
            },
            menuButton: {
                marginRight: theme.spacing(2),
                },
                title: {
                flexGrow: 1,
                display: 'none',
                [theme.breakpoints.up('sm')]: {
                    display: 'block',
                },
                },
                search: {
                position: 'relative',
                borderRadius: theme.shape.borderRadius,
                backgroundColor: fade(theme.palette.common.white, 0.15),
                '&:hover': {
                    backgroundColor: fade(theme.palette.common.white, 0.25),
                },
                marginLeft: 0,
                width: '100%',
                [theme.breakpoints.up('sm')]: {
                    marginLeft: theme.spacing(1),
                    width: 'auto',
                },
                },
                searchIcon: {
                padding: theme.spacing(0, 2),
                height: '100%',
                position: 'absolute',
                pointerEvents: 'none',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center',
                },
                inputRoot: {
                color: 'inherit',
                },
                inputInput: {
                padding: theme.spacing(1, 1, 1, 0),
                // vertical padding + font size from searchIcon
                paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
                transition: theme.transitions.create('width'),
                width: '100%',
                [theme.breakpoints.up('sm')]: {
                    width: '12ch',
                    '&:focus': {
                    width: '20ch',
                    },
                },
                },
                profileDialog:{
                    width: '100%',
                    maxWidth: '100%',
                }
            }));

            const classes = useStyles();

            const handleRequestSort = (event, property) => {
                const isAsc = orderBy === property && order === 'asc';
                setOrder(isAsc ? 'desc' : 'asc');
                setOrderBy(property);
            };

            const handleSelectAllClick = (event) => {
                if (event.target.checked) {
                const newSelecteds = rows.map((n) => n.id);
                setSelected(newSelecteds);
                return;
                }
                setSelected([]);
            };
            let timeoutVar;
            const handleSearch = async (event) => {
                const val = event.target.value;
                clearTimeout(timeoutVar);
                timeoutVar = setTimeout(async function() {
                    let headers = new Headers();
                    headers.append(`Authorization`,`Bearer ${sessionStorage.getItem("token")}`);
                    let params = {
                        orderby : 'id',
                        ordertype : 'asc',
                        offset : 0,
                        limit : 100,
                        expression: `[{"Field": "firstname", "FilterType":"Contains","Value":"${val}"}]`,
                    }
                    let res = await DataProvider.GET('persons/get',headers, params);
                    if(typeof res === `undefined`) return;
                    setRowsData(res.data);
                },2000);
            }

            const handleClick = (event, id) => {
                
                const selectedIndex = selected.indexOf(id);
                let newSelected = [];

                if (selectedIndex === -1) {
                newSelected = newSelected.concat(selected, id);
                } else if (selectedIndex === 0) {
                newSelected = newSelected.concat(selected.slice(1));
                } else if (selectedIndex === selected.length - 1) {
                newSelected = newSelected.concat(selected.slice(0, -1));
                } else if (selectedIndex > 0) {
                newSelected = newSelected.concat(
                    selected.slice(0, selectedIndex),
                    selected.slice(selectedIndex + 1),
                );
                }

                setSelected(newSelected);
            };

            const handleChangePage = (event, newPage) => {
                setPage(newPage);
            };

            const handleChangeRowsPerPage = (event) => {
                setRowsPerPage(parseInt(event.target.value, 10));
                setPage(0);
            };

            const handleChangeDense = (event) => {
                setDense(event.target.checked);
            };


            const handleAddClick = ()=>{
                setAddDialog(true);
                setGender(1);
            }

            const handleDeleteClick = async () =>{
                let anySelected = false;
            for(let i of selected){
                anySelected = true;
                let headers = {
                    'authorization' : `Bearer ${sessionStorage.getItem("token")}`
                }
                let formdata = new FormData();
                formdata.append("personid", i)
                let res = await DataProvider.POST('persons/delete',headers,formdata);
                if(typeof res === 'undefined') return;
            }
            ///////////////////////Read data again
            if(anySelected === true){
                let headers = {
                    'authorization' : `Bearer ${sessionStorage.getItem('token')}`
                };
                let params = {
                    orderby : 'id',
                    ordertype : 'asc',
                    offset : 0,
                    limit: 100
                }
                const res = await DataProvider.GET('persons/get',headers,params);
                if(typeof res === 'undefined') return;
                let tempArr = [];
                for(let elm of res.data){
                    tempArr.push({'id': elm.id,'firstName': elm.firstName, 'lastName': elm.lastName, 'gender': elm.gender ,'fatherName': elm.fatherName, 'birthDate': elm.birthDate, 'birthPlace': elm.birthPlace, 'startDate': elm.startDate, 'endDate': elm.endDate});
                }
                setRowsData(tempArr)
                setSelected([]);
            }
            // let tempArr =[];
            // for(let elm of rows){
            //     if(!selected.includes(elm.firstName)) tempArr.push(elm)
            // }

            // setRowsData(tempArr);
            
            }

            const handleEditChange = param => event => {
                switch (param){
                    case "firstName":
                        setFirstName(event.target.value);
                        if (!event.target.value.length) {
                            setFirstNameError(true);
                            setFirstNameErrorMessage('نام کوچک باید وارد شود!');
                        } else {
                            setFirstNameError(false);
                            setFirstNameErrorMessage('');
                        }

                        break;
                    case "lastName":
                        setLastName(event.target.value);
                        if (!event.target.value.length) {
                            setLastNameError(true);
                            setLastNameErrorMessage('نام خانوادگی باید وارد شود!');
                        } else {
                            setLastNameError(false);
                            setLastNameErrorMessage('');
                        }
                        break;
                    case "gender":
                        setGender(event.target.value);
                        break;
                    case "fatherName":
                        setFatherName(event.target.value);
                        if (!event.target.value.length) {
                            setFatherError(true);
                            setFatherErrorMessage('نام پدر باید وارد شود!');
                        } else {
                            setFatherError(false);
                            setFatherErrorMessage('');
                        }
                        break;
                    case "birthPlace":
                        setBirthPlace(event.target.value);
                        if(!event.target.value.length){
                            setBirthPlaceError(true);
                            setBirthPlaceErrorMessage('محل تولد باید وارد شود!');
                        }
                        else{
                            setBirthPlaceError(false);
                            setBirthPlaceErrorMessage('');
                        }
                        break;
                    default:
                        break;
                }
            }

            const handleChange = param => event => {
                switch (param){
                    case "firstName":
                        setFirstName(event.target.value);
                        if (!event.target.value.length) {
                            setFirstNameError(true);
                            setFirstNameErrorMessage('نام کوچک باید وارد شود!');
                        } else {
                            setFirstNameError(false);
                            setFirstNameErrorMessage('');
                        }
                        break;
                    case "lastName":
                        setLastName(event.target.value);
                        if (!event.target.value.length) {
                            setLastNameError(true);
                            setLastNameErrorMessage('نام خانوادگی باید وارد شود!');
                        } else {
                            setLastNameError(false);
                            setLastNameErrorMessage('');
                        }
                        break;
                    case "gender":
                        setGender(event.target.value);
                        break;
                    case "fatherName":
                        setFatherName(event.target.value);
                        if (!event.target.value.length) {
                            setFatherError(true);
                            setFatherErrorMessage('نام پدر باید وارد شود!');
                        } else {
                            setFatherError(false);
                            setFatherErrorMessage('');
                        }
                        break;
                    case "birthPlace":
                        setBirthPlace(event.target.value);
                        if(!event.target.value.length){
                            setBirthPlaceError(true);
                            setBirthPlaceErrorMessage('محل تولد باید وارد شود!');
                        }
                        else{
                            setBirthPlaceError(false);
                            setBirthPlaceErrorMessage('');
                        }
                        break;
                    default:
                        break;
                }
            }


            const setBirthDateFunction = (value)=>{
                setBirthDate(value._d.toISOString());
            }

            const setStartDateFunction = (value) => {
                setStartDate(value._d.toISOString());
            }

            const setEndDateFunction = (value) =>{
                value === null ? setEndDate(undefined) : setEndDate(value._d.toISOString());
            }
            const handleEditClick = ()=> {
                setEditDialog(true);
                let selectedRow = rows.find((value,index,array)=>{
                    return value.id === selected[0];
                });
                setFirstName(selectedRow.firstName);
                setLastName(selectedRow.lastName);
                setGender(selectedRow.gender);
                setFatherName(selectedRow.fatherName);
                setBirthDate(selectedRow.birthDate);
                setBirthPlace(selectedRow.birthPlace);
                setStartDate(selectedRow.startDate);
                setEndDate(selectedRow.endDate === null ? undefined : selectedRow.endDate);
            }

            const handleClose = ()=>{
                setAddDialog(false);
                setEditDialog(false);
                setFirstName('');
                setLastName('');
                setFatherName('');
                setBirthDate(undefined);
                setBirthPlace('');
                setStartDate(undefined);
                setEndDate(undefined);
                setFirstNameError(false);
                setFirstNameErrorMessage('');
                setLastNameError(false);
                setLastNameErrorMessage('');
                setFatherError(false);
                setFatherErrorMessage('');
                setBirthPlaceError(false);
                setBirthPlaceErrorMessage('');
            }

            const handleEditSubmit = async () =>{
                console.log(`firstName:${firstName},lastName:${lastName},gender:${gender},fatherName:${fatherName},birthDate:${birthDate},birthPlace:${birthPlace},startDate:${startDate},endDate:${endDate}`);
                let mustReturn = false;
                if(firstNameError || !firstName.length){
                    setFirstNameError(true);
                    setFirstNameErrorMessage('نام کوچک باید وارد شود!');
                    mustReturn = true;
                } 
                if (lastNameError || !lastName.length){
                    setLastNameError(true);
                    setLastNameErrorMessage('نام خانوادگی باید وارد شود!');
                    mustReturn = true;
                } 
                if (fatherError || !fatherName.length){
                    setFatherError(true);
                    setFatherErrorMessage('نام پدر باید وارد شود!');
                    mustReturn = true;
                } 
                if (birthPlaceError || !birthPlace.length){
                    setBirthPlaceError(true);
                    setBirthPlaceErrorMessage('محل تولد باید وارد شود!');
                    mustReturn = true;
                }
                
                if(typeof birthDate === 'undefined'){
                    console.log('birthday empty error');
                    enqueueSnackbar('تاریخ تولد وارد شود!', {variant: 'error'});
                    mustReturn = true;
                }else if ((new Date(birthDate)) >= (new Date())){
                    console.log('birthday bigger than today error');
                    enqueueSnackbar('تاریخ تولد تصحیح شود!', {variant: 'error'});
                    mustReturn = true;
                } 

                if(typeof startDate === 'undefined'){
                    console.log('start date empty error');
                    enqueueSnackbar('تاریخ شروع وارد شود!', {variant: 'error'});
                    mustReturn = true;
                }else if ((new Date(startDate)) >= (new Date())){
                    console.log('start date bigger than today error');
                    enqueueSnackbar('تاریخ شروع تصحیح شود!', {variant: 'error'});
                    mustReturn = true;
                } 
                if(typeof endDate !== 'undefined' && new Date(startDate) >= new Date(endDate)){
                    console.log('start date bigger then end date error');
                    enqueueSnackbar('تاریخ شروع یا پایان تصحیح شود!', {variant: 'error'});
                    mustReturn = true;
                }
                    
                if(mustReturn){
                    return;
                }

                let headers = {
                    'authorization' : `Bearer ${sessionStorage.getItem("token")}`
                }
                let formdata = new FormData();
                formdata.append("personid", selected[0]);
                formdata.append("entitybaseid", 1);
                formdata.append("firstname", firstName);
                formdata.append("lastname", lastName);
                formdata.append("gender", gender === 1 ? 'Male' : 'Female');
                formdata.append("fatherName", fatherName);
                formdata.append("birthDate", new Date(birthDate).toISOString());
                formdata.append("birthPlace", birthPlace);
                formdata.append("startDate", new Date(startDate).toISOString());
                formdata.append("endDate", typeof(endDate) === 'undefined' ? '' : new Date(endDate).toISOString());
                let res = await DataProvider.POST('persons/put',headers,formdata);
                if(typeof res === 'undefined') return;
                setAddDialog(false);
                setEditDialog(false);
                ////read data again
                headers = {
                    'authorization' : `Bearer ${sessionStorage.getItem('token')}`
                };
                let params = {
                    orderby : 'id',
                    ordertype : 'asc',
                    offset : 0,
                    limit: 100
                }
                res = await DataProvider.GET('persons/get',headers,params);
                if(typeof res === 'undefined') return;
                let tempArr = [];
                for(let elm of res.data){
                    tempArr.push({'id': elm.id,'firstName': elm.firstName, 'lastName': elm.lastName, 'gender': elm.gender ,'fatherName': elm.fatherName, 'birthDate': elm.birthDate, 'birthPlace': elm.birthPlace, 'startDate': elm.startDate, 'endDate': elm.endDate});
                }
                enqueueSnackbar('اطلاعات با موفقیت ویرایش و ثبت شد!', {variant: 'success'});
                setRowsData(tempArr)
            }

            const correctDate = (date)=>{
                if(typeof date === 'undefined' || date === null) {
                    return;
                }
                return momentjalaali(date,'YYYY-MM-DD HH:mm:ss').format('jYYYY/jMM/jD HH:mm:ss');
            }

            const handleAddSubmit = async () => {
                console.log(`firstName:${firstName},lastName:${lastName},gender:${gender},fatherName:${fatherName},birthDate:${birthDate},birthPlace:${birthPlace},startDate:${startDate},endDate:${endDate}`);
                let mustReturn = false;
                if(firstNameError || !firstName.length){
                    setFirstNameError(true);
                    setFirstNameErrorMessage('نام کوچک باید وارد شود!');
                    mustReturn = true;
                } 
                if (lastNameError || !lastName.length){
                    setLastNameError(true);
                    setLastNameErrorMessage('نام خانوادگی باید وارد شود!');
                    mustReturn = true;
                } 
                if (fatherError || !fatherName.length){
                    setFatherError(true);
                    setFatherErrorMessage('نام پدر باید وارد شود!');
                    mustReturn = true;
                } 
                if (birthPlaceError || !birthPlace.length){
                    setBirthPlaceError(true);
                    setBirthPlaceErrorMessage('محل تولد باید وارد شود!');
                    mustReturn = true;
                }
                if(typeof birthDate === 'undefined'){
                    console.log('birthday empty error');
                    enqueueSnackbar('تاریخ تولد وارد شود!', {variant: 'error'});
                    mustReturn = true;
                }else if ((new Date(birthDate)) >= (new Date())){
                    console.log('birthday bigger than today error');
                    enqueueSnackbar('تاریخ تولد تصحیح شود!', {variant: 'error'});
                    mustReturn = true;
                } 

                if(typeof startDate === 'undefined'){
                    console.log('start date empty error');
                    enqueueSnackbar('تاریخ شروع وارد شود!', {variant: 'error'});
                    mustReturn = true;
                }else if ((new Date(startDate)) >= (new Date())){
                    console.log('start date bigger than today error');
                    enqueueSnackbar('تاریخ شروع تصحیح شود!', {variant: 'error'});
                    mustReturn = true;
                } 
                
                if(typeof endDate !== 'undefined' && new Date(startDate) >= new Date(endDate)){
                    console.log('start date bigger then end date error');
                    enqueueSnackbar('تاریخ شروع یا پایان تصحیح شود!', {variant: 'error'});
                    mustReturn = true;
                }
                    
                if(mustReturn){
                    return;
                }

                // console.log(`firstName:${firstName},lastName:${lastName},gender:${gender},fatherName:${fatherName},birthDate:${birthDate},birthPlace:${birthPlace},startDate:${startDate},endDate:${endDate}`);

                let headers = {
                    'authorization' : `Bearer ${sessionStorage.getItem("token")}`
                }
                let formdata = new FormData();
                formdata.append("entitybaseid", 1);
                formdata.append("firstname", firstName);
                formdata.append("lastname", lastName);
                formdata.append("gender", gender === 1 ? 'Male' : 'Female');
                formdata.append("fatherName", fatherName);
                formdata.append("birthDate", new Date(birthDate).toISOString());
                formdata.append("birthPlace", birthPlace);
                formdata.append("startDate", new Date(startDate).toISOString());
                formdata.append("endDate", typeof(endDate) === 'undefined' ? '' : new Date(endDate).toISOString());
                let res = await DataProvider.POST('persons/put',headers,formdata);
                if(typeof res === 'undefined') return;
                setAddDialog(false);
                setEditDialog(false);
                ////read data again
                headers = {
                    'authorization' : `Bearer ${sessionStorage.getItem('token')}`
                };
                let params = {
                    orderby : 'id',
                    ordertype : 'asc',
                    offset : 0,
                    limit: 100
                }
                res = await DataProvider.GET('persons/get',headers,params);
                if(typeof res === 'undefined') return;
                let tempArr = [];
                for(let elm of res.data){
                    tempArr.push({'id': elm.id,'firstName': elm.firstName, 'lastName': elm.lastName, 'gender': elm.gender ,'fatherName': elm.fatherName, 'birthDate': elm.birthDate, 'birthPlace': elm.birthPlace, 'startDate': elm.startDate, 'endDate': elm.endDate});
                }
                enqueueSnackbar('اطلاعات با موفقیت ثبت شد!', {variant: 'success'});
                setRowsData(tempArr)
            }
            const isSelected = (name) => selected.indexOf(name) !== -1;

            const emptyRows = rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);
    return ( 
        <Fragment >
                <Grid container justify="center" className={classes.p100}>
                    <Grid container item sm={10} xs={12} justify="space-between" direction="row-reverse" className="tableHeader">
                        <Grid container sm={4} xs={12} item justify="flex-start" direction="row-reverse" >
                                <Tooltip onClick={()=>handleAddClick()} title={
                                    <Typography>
                                        اضافه کردن شخص حقیقی
                                    </Typography>
                                }>
                                    <IconButton>
                                        <AddBox />
                                    </IconButton>
                                </Tooltip>
                                <Tooltip  title={
                                    <Typography>
                                        ویرایش شخص حقیقی
                                    </Typography>
                                }>
                                <span>
                                    <IconButton disabled={selected.length !== 1} onClick={handleEditClick}>
                                        <Edit />
                                    </IconButton>
                                </span>
                                </Tooltip>
                                <Tooltip  title={
                                    <Typography>
                                        حذف شخص حقیقی
                                    </Typography>
                                }>
                                <span>
                                    <IconButton disabled={selected.length === 0} onClick={handleDeleteClick}>
                                        <Delete />
                                    </IconButton>
                                </span>
                                </Tooltip>
                               
                        </Grid>
                        <Grid container item sm={4} xs={12}>
                                <div className={classes.search}>
                                    <div className={classes.searchIcon}>
                                    <Search />
                                    </div>
                                    <InputBase
                                    placeholder="جستجو"
                                    classes={{
                                        root: classes.inputRoot,
                                        input: classes.inputInput,
                                    }}
                                    inputProps={{ 'aria-label': 'search' }}
                                    onChange={handleSearch}
                                    />
                                </div>
                        </Grid>
                    </Grid>
                    <Grid container item sm={10}>
                        <div className={classes.root}>
                            <Paper className={classes.paper}>
                            {/* <EnhancedTableToolbar numSelected={selected.length} /> */}
                            <TableContainer>
                                <Table
                                className={classes.table}
                                aria-labelledby="tableTitle"
                                size={dense ? 'small' : 'medium'}
                                aria-label="enhanced table"
                                >
                                <EnhancedTableHead
                                    classes={classes}
                                    numSelected={selected.length}
                                    order={order}
                                    orderBy={orderBy}
                                    onSelectAllClick={handleSelectAllClick}
                                    onRequestSort={handleRequestSort}
                                    rowCount={rows.length}
                                />
                                <TableBody>
                                    {stableSort(rows, getComparator(order, orderBy))
                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                    .map((row, index) => {
                                        const isItemSelected = isSelected(row.id);
                                        const labelId = `enhanced-table-checkbox-${index}`;
                    
                                        return (
                                        <TableRow
                                            hover
                                            onClick={(event) => handleClick(event, row.id)}
                                            role="checkbox"
                                            aria-checked={isItemSelected}
                                            tabIndex={-1}
                                            key={row.id}
                                            selected={isItemSelected}
                                        >
                                            <TableCell padding="checkbox">
                                            <Checkbox
                                                checked={isItemSelected}
                                                inputProps={{ 'aria-labelledby': labelId }}
                                            />
                                            </TableCell>
                                            <TableCell component="th" id={labelId} scope="row" padding="none">
                                            {row.id}
                                            </TableCell>
                                            <TableCell align="right">{row.firstName}</TableCell>
                                            <TableCell align="right">{row.lastName}</TableCell>
                                            <TableCell align="right">{row.gender === 'Male'?'مرد':'زن'}</TableCell>
                                            <TableCell align="right">{row.fatherName}</TableCell>
                                            <TableCell align="right">{correctDate(row.birthDate)}</TableCell>
                                            <TableCell align="right">{row.birthPlace}</TableCell>
                                            <TableCell align="right">{correctDate(row.startDate)}</TableCell>
                                            <TableCell align="right">{correctDate(row.endDate)}</TableCell>
                                            <TableCell align="right"><Tooltip onClick={(event,rowid)=>handleOperationButtons(event,row.id)} title={<Typography>نمایش پروفایل</Typography>}><IconButton><PersonIcon /></IconButton></Tooltip></TableCell>
                                        </TableRow>
                                        );
                                    })}
                                    {emptyRows > 0 && (
                                    <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                                        <TableCell colSpan={6} />
                                    </TableRow>
                                    )}
                                </TableBody>
                                </Table>
                            </TableContainer>
                            <TablePagination
                                rowsPerPageOptions={[5, 10, 25]}
                                component="div"
                                count={rows.length}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                onChangePage={handleChangePage}
                                onChangeRowsPerPage={handleChangeRowsPerPage}
                            />
                            </Paper>
                            <FormControlLabel
                            control={<Switch checked={dense} onChange={handleChangeDense} />}
                            label="جدول فشرده"
                            />
                        </div>
                    </Grid>
                </Grid>
                <Dialog open={addDialog} onClose={handleClose} aria-labelledby="form-dialog-title" className={classes.dialogRtl}>
                    <DialogTitle id="form-dialog-title">اضافه کردن شخص حقیقی</DialogTitle>
                    <DialogContent>
                    <DialogContentText>
                        
                    </DialogContentText>
                    <Grid container justify="space-around">
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    نام
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <TextField
                                    error={firstNameError}
                                    id="firstName"
                                    onChange={handleChange('firstName')}
                                    helperText={firstNameErrorMessage}
                                />
                            </Grid>
                        </Grid>
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    نام خانوادگی
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <TextField
                                    error={lastNameError}
                                    id="lastName"
                                    onChange={handleChange('lastName')}
                                    helperText={lastNameErrorMessage}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container justify="space-around">
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    جنسیت
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={gender}
                                    onChange={handleChange('gender')}
                                    >
                                    <MenuItem value={1}>مرد</MenuItem>
                                    <MenuItem value={0}>زن</MenuItem>
                                </Select>
                            </Grid>
                        </Grid>
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    نام پدر
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <TextField
                                    error={fatherError}
                                    id="fatherName"
                                    onChange={handleChange('fatherName')}
                                    helperText={fatherErrorMessage}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container justify="space-around">
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    تاریخ تولد
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                            <DatePicker
                                // error={birthDateError}
                                isGregorian={false}
                                onChange={value => setBirthDateFunction(value)}
                                // helperText={birthDateErrorMessage}
                                />
                            </Grid>
                        </Grid>
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    محل تولد
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <TextField
                                    error={birthPlaceError}
                                    id="birthPlace"
                                    onChange={handleChange('birthPlace')}
                                    helperText={birthPlaceErrorMessage}
                                />
                        </Grid>
                        </Grid>
                    </Grid>
                    <Grid container justify="space-around">
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    تاریخ شروع
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <DatePicker
                                    isGregorian={false}
                                    onChange={value => setStartDateFunction(value)}
                                    style={{
                                        zIndex : 5100
                                    }}
                                />
                            </Grid>
                        </Grid>
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    تاریخ پایان
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <DatePicker
                                    isGregorian={false}
                                    onChange={value => setEndDateFunction(value)}
                                    style={{
                                        zIndex : 5100
                                    }}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        لغو
                    </Button>
                    <Button onClick={handleAddSubmit} color="primary">
                        ثبت
                    </Button>
                    </DialogActions>
                </Dialog>
                <Dialog open={editDialog} onClose={handleClose} aria-labelledby="form-dialog-title" className={classes.dialogRtl}>
                    <DialogTitle id="form-dialog-title">ویرایش شخص حقیقی</DialogTitle>
                    <DialogContent>
                    <DialogContentText>
                        
                    </DialogContentText>
                    <Grid container justify="space-around">
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    نام
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <TextField
                                    error={firstNameError}
                                    id="firstName"
                                    onChange={handleEditChange('firstName')}
                                    helperText={firstNameErrorMessage}
                                    value={firstName}
                                />
                            </Grid>
                        </Grid>
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    نام خانوادگی
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <TextField
                                    error={lastNameError}
                                    id="lastName"
                                    onChange={handleEditChange('lastName')}
                                    helperText={lastNameErrorMessage}
                                    value={lastName}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container justify="space-around">
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    جنسیت
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={gender === 'Male' ? 1 : 0}
                                    onChange={handleEditChange('gender')}
                                    >
                                    <MenuItem value={1}>مرد</MenuItem>
                                    <MenuItem value={0}>زن</MenuItem>
                                </Select>
                            </Grid>
                        </Grid>
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    نام پدر
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <TextField
                                    error={fatherError}
                                    id="fatherName"
                                    onChange={handleEditChange('fatherName')}
                                    helperText={fatherErrorMessage}
                                    value={fatherName}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container justify="space-around">
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    تاریخ تولد
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                            <DatePicker
                                // error={birthDateError}
                                isGregorian={false}
                                onChange={value => setBirthDateFunction(value)}
                                value={momentjalaali(birthDate)}
                                // helperText={birthDateErrorMessage}
                                />
                            </Grid>
                        </Grid>
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    محل تولد
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <TextField
                                    error={birthPlaceError}
                                    id="birthPlace"
                                    onChange={handleEditChange('birthPlace')}
                                    helperText={birthPlaceErrorMessage}
                                    value={birthPlace}
                                />
                        </Grid>
                        </Grid>
                    </Grid>
                    <Grid container justify="space-around">
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    تاریخ شروع
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <DatePicker
                                    isGregorian={false}
                                    onChange={value => setStartDateFunction(value)}
                                    style={{
                                        zIndex : 5100
                                    }}
                                    value={momentjalaali(startDate)}
                                />
                            </Grid>
                        </Grid>
                        <Grid container item sm={6}>
                            <Grid container item sm={12}>
                                <Typography>
                                    تاریخ پایان
                                </Typography>
                            </Grid>
                            <Grid container item sm={12}>
                                <DatePicker
                                    isGregorian={false}
                                    // onChange={value => setEndDateFunction(value)}
                                    style={{
                                        zIndex : 5100
                                    }}
                                    value={endDate === undefined ? undefined : momentjalaali(endDate)}
                                />
                            </Grid>
                        </Grid>
                    </Grid>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        لغو
                    </Button>
                    <Button onClick={handleEditSubmit} color="primary">
                        ثبت
                    </Button>
                    </DialogActions>
                </Dialog>
                <ProfileDialogPerson open={openProfileDialog}
                                     currentUser={currentUser}
                                     onCloseProfileDialog={handleCloseProfileDialog}
                                     />
        </Fragment>
    );

    
}
