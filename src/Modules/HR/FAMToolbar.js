import React,{Component,Fragment} from 'react';
import {Grid,Tooltip,Typography,IconButton,InputBase} from '@material-ui/core';
import {Search} from '@material-ui/icons';
import {withStyles, fade} from '@material-ui/core/styles';
const myStyle = theme => ({
        search: {
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: fade(theme.palette.common.white, 0.25),
            },
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(1),
                width: 'auto',
            },
        },
        searchIcon: {
            padding: theme.spacing(0, 2),
            height: '100%',
            position: 'absolute',
            pointerEvents: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
            inputRoot: {
            color: 'inherit',
        },
            inputInput: {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                width: '12ch',
                '&:focus': {
                width: '20ch',
                },
            },
        },
})

class FAMToolbar extends Component {
    state = {

    }

    handleSearch = () => {

    }

    render(){
        const {classes} = this.props;
        return (
            <Fragment>
                <Grid container justify="center" className={classes.p100} style={{direction: `${this.props.direction}`}}>
                    <Grid container item sm={12} xs={12} justify="space-between" direction="row-reverse" className="tableHeader">
                        <Grid container sm={4} xs={12} item justify="flex-start" direction="row-reverse" >
                            {this.props.items.map(value=> 
                                    <Tooltip 
                                        onClick={value.clickEvent}    
                                        title={
                                        <Typography>
                                            {value.name}
                                        </Typography>}>
                                        <IconButton> 
                                            <i className={value.icon}></i> 
                                            {/* <i class="fa fa-address-book" aria-hidden="true"></i> */}
                                        </IconButton>
                                    </Tooltip>)
                                }
                        </Grid>
                        <Grid container item sm={4} xs={12} >
                                <div className={classes.search} style={{display: this.props.searchInput === false ? 'none' : 'unset'}}>
                                    <div className={classes.searchIcon}>
                                    <Search />
                                    </div>
                                    <InputBase
                                    placeholder="جستجو"
                                    classes={{
                                        root: classes.inputRoot,
                                        input: classes.inputInput,
                                    }}
                                    inputProps={{ 'aria-label': 'search' }}
                                    onChange={this.handleSearch}
                                    />
                                </div>
                        </Grid>
                    </Grid>
                </Grid>
            </Fragment>
        )
    }
}

export default withStyles(myStyle)(FAMToolbar);