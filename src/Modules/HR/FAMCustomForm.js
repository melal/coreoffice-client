import React, { Component, Fragment} from 'react';
import {Divider,Paper,Grid,Tooltip,InputBase, Typography,Button, TextField, IconButton, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle    } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import AddIcon from '@material-ui/icons/Add';
import {withStyles} from '@material-ui/core/styles';
import {DataProvider} from './DataProvider';
import DatePicker from 'react-datepicker2';
import momentjalaali from 'moment-jalaali';
import Autocomplete from '@material-ui/lab/Autocomplete'; 


const myStyle = theme => ({

    dialogRtl:{
        direction: 'rtl',
    },
    dialogTitle:{
        backgroundColor: '#b2b2ff',
        fontFamily: 'Amiri' ,
    },
    dialogTitleRoot: {
        fontSize: 'large',
        padding: '3px 24px'
    },
    paperRoot: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width:'100%'
        
    },input: {
        marginLeft: theme.spacing(1),
        flex: 1,
    },
    divider: {
        height: 28,
        margin: 4,
    },
    gridItem: {
        padding: '10px 10px'
    },
    gridRow: {
        marginBottom: '10px'    
    },
    autocompleteRoot: {
        width : '100%'
    }

})
var firstTimeout;
var secondTimeout;
class FAMCustomForm extends Component {
    state = { 
        openCustomForm: this.props.openCustomForm,
        firstInputDate: [],
        secondInputDate: [],
    };

    componentDidMount() {

    }


    setFromDateFunction = (value) => {

    }

    setEndDateFunction = (value) => {

    }
    
    handleOnChangeOne = (event,value,reason) => {
        let val;
        if(value.length === 0) {
            this.setState({firstInputDate: []});
        }
        else val = value;
        clearTimeout(firstTimeout);
        firstTimeout = setTimeout(async () => {
            let headers = new Headers();
            headers.append(`Authorization`,`Bearer ${sessionStorage.getItem("token")}`);
            let params = {
                orderby : 'id',
                ordertype : 'asc',
                offset : 0,
                limit : 100,
                expression: `[{"Field": "firstname", "FilterType":"Contains","Value":"${val}"}]`,
            }
            let res = await DataProvider.GET('persons/get',headers, params);
            if(typeof res === `undefined`) return;
            this.setState({firstInputDate: res.data});
        },1000);
    }

    handleOnChangeTwo = (event,value,reason) => {
        let val;
        if(value.length === 0) {
            this.setState({secondInputDate: []});
        }
        else val = value;
        clearTimeout(secondTimeout);
        secondTimeout = setTimeout(async () => {
            let headers = new Headers();
            headers.append(`Authorization`,`Bearer ${sessionStorage.getItem("token")}`);
            let params = {
                orderby : 'id',
                ordertype : 'asc',
                offset : 0,
                limit : 100,
                expression: `[{"Field": "firstname", "FilterType":"Contains","Value":"${val}"}]`,
            }
            let res = await DataProvider.GET('persons/get',headers, params);
            if(typeof res === `undefined`) return;
            this.setState({secondInputDate: res.data});
        },1000)
    }

    render() { 
        const {classes} = this.props;
        return (
            <Fragment>
                    <Dialog 
                            open={this.props.openCustomForm} 
                            className={classes.dialogRtl}
                            maxWidth='lg'
                            >
                    <DialogTitle id="form-dialog-title"
                                 className={classes.dialogTitle}
                                 classes={{
                                     root:classes.dialogTitleRoot
                                 }}
                    >
                        {this.props.title}
                    </DialogTitle>
                    <DialogContent>
                    <DialogContentText>
                            {this.props.description}
                    </DialogContentText>
                    <Grid container>
                        <Grid container item lg={12} className={classes.gridRow}>
                            <Grid container item lg={6} className={classes.gridItem}>
                                <Typography>
                                    انتخاب شخص حقیقی
                                </Typography>
                                <Paper component="form" className={classes.paperRoot}>
                                    <Autocomplete
                                        id="combo-box-1"
                                        onInputChange={(event,value,reason) => this.handleOnChangeOne(event,value,reason)}
                                        options={this.state.firstInputDate}
                                        getOptionLabel={(option) => option.firstName}
                                        noOptionsText=''
                                        renderInput={(params) => <TextField {...params} variant="outlined" />}
                                        classes={{
                                            root: classes.autocompleteRoot
                                        }}
                                    />
                                    <Tooltip 
                                        title={<Typography>
                                                    جستجوی پیشرفته
                                            </Typography>
                                            }>
                                            <IconButton 
                                                size="small"
                                            >
                                                <SearchIcon />
                                            </IconButton>  
                                        
                                    </Tooltip>
                                    <Tooltip
                                        title={<Typography>
                                                        انتخاب
                                            </Typography>
                                            }>
                                                <IconButton
                                                    size="small"
                                                >
                                                    <ArrowDropDownIcon />
                                                </IconButton>
                                    </Tooltip>
                                    <Tooltip 
                                        title={
                                            <Typography>
                                                    اضافه کردن
                                            </Typography>
                                        }>
                                            <IconButton
                                                size="small"
                                            >
                                                <AddIcon />
                                            </IconButton>
                                    </Tooltip>
                                </Paper>
                            </Grid>
                            <Grid container item lg={6} className={classes.gridItem}>
                                <Typography>
                                        انتخاب شخص (حقیقی/حقوقی)
                                </Typography>
                                <Paper component="form" className={classes.paperRoot}>
                                <Autocomplete
                                        id="combo-box-2"
                                        options={this.state.secondInputDate}
                                        getOptionLabel={(option) => option.firstName}
                                        noOptionsText=''
                                        renderInput={(params) => <TextField {...params} variant="outlined" />}
                                        classes={{
                                            root: classes.autocompleteRoot
                                        }}
                                        onInputChange={(event,value,reason) => this.handleOnChangeTwo(event,value,reason)}
                                    />
                                    <Tooltip 
                                        title={<Typography>
                                                    جستجوی پیشرفته
                                            </Typography>
                                            }>
                                            <IconButton 
                                                size="small"
                                            >
                                                <SearchIcon />
                                            </IconButton>  
                                        
                                    </Tooltip>
                                    <Tooltip
                                        title={<Typography>
                                                        انتخاب
                                            </Typography>
                                            }>
                                                <IconButton
                                                    size="small"
                                                >
                                                    <ArrowDropDownIcon />
                                                </IconButton>
                                    </Tooltip>
                                    <Tooltip 
                                        title={
                                            <Typography>
                                                    اضافه کردن
                                            </Typography>
                                        }>
                                                <IconButton
                                                            size="small"
                                                >
                                            <AddIcon />
                                        </IconButton>
                                    </Tooltip>
                                </Paper>
                            </Grid>
                        </Grid>
                        <Grid container item lg={12} className={classes.gridRow}>
                            <Grid container item lg={6} className={classes.gridItem}>
                                <Typography>
                                        انتخاب نوع رابط
                                </Typography>
                                <Paper component="form" className={classes.paperRoot}>
                                    <InputBase 
                                        className={classes.input}
                                    />
                                    <Tooltip
                                        title={<Typography>
                                                        انتخاب
                                            </Typography>
                                            }>
                                                <IconButton
                                                    size="small"
                                                >
                                                    <ArrowDropDownIcon />
                                                </IconButton>
                                    </Tooltip>
                                </Paper>
                            </Grid>
                        </Grid>
                        <Grid container item lg={12} className={classes.gridRow}>
                            <Grid container item lg={6} className={classes.gridItem}>
                                <Typography>
                                        از تاریخ
                                </Typography>
                                <DatePicker 
                                    isGregorian={false}
                                    onChange={value => this.setFromDateFunction(value)}
                                />
                            </Grid>
                            <Grid container item lg={6} className={classes.gridItem}>
                                <Typography>
                                        تا تاریخ
                                </Typography>
                                <DatePicker 
                                    isGregorian={false}
                                    onChange={value => this.setEndDateFunction(value)}
                                />  
                            </Grid>
                        </Grid>
                    </Grid>
                    </DialogContent>
                    <DialogActions>
                            {this.props.cancelButton === true ? 
                            <Button 
                                color="primary"
                                onClick={()=>this.props.onCancelClick()}
                                >
                            لغو
                            </Button> : ''}
                            {this.props.submitButton === true ? 
                            <Button color="primary">
                                ثبت
                            </Button> : ''}
                    </DialogActions>
                </Dialog>
            </Fragment> 
        );
    }
}
 
export default withStyles(myStyle)(FAMCustomForm);