import React, { Component, Fragment } from "react";
import {
  Snackbar,
  InputLabel,
  AppBar,
  Select,
  MenuItem,
  Typography,
  Grid,
  IconButton,
  Tooltip,
  InputBase,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TableSortLabel,
  Checkbox,
  TablePagination,
  Toolbar,
  FormControlLabel,
  Switch,
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@material-ui/core";
import { AddBox, Edit, Delete, Search } from "@material-ui/icons";
import CloseIcon from "@material-ui/icons/Close";
import { withStyles, fade } from "@material-ui/core/styles";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import FAMToolbar from "./FAMToolbar";
import { DataProvider } from "../../DataProvider";
import DatePicker from "react-datepicker2";
import momentjalaali from "moment-jalaali";
import { Form } from "react-formio";
import { formData } from "./tempData";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

//   TabPanel.propTypes = {
//     children: PropTypes.node,
//     index: PropTypes.any.isRequired,
//     value: PropTypes.any.isRequired,
//   };

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const myStyle = (theme) => ({
  root: {
    width: "100%",
  },
  p100: {
    paddingTop: "100px",
  },
  mr2: {
    marginRight: "20px",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
    direction: "rtl",
  },
  dialogRtl: {
    direction: "rtl",
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "12ch",
      "&:focus": {
        width: "20ch",
      },
    },
  },
  profileDialog: {
    width: "100%",
    maxWidth: "100%",
  },
});

class ProfileDialogPerson extends Component {
  state = {
    tabPanelValue: 0,
    addFeatureDialog: false,
    featuresList: [],
    selectedFeature: "",
    selectedFeatureType: "",
    selectedFeatureId: "",
    selectedFeatureData: "",
    formTextInput: "",
    datePickerTextFieldFormStartDate: undefined,
    datePickerTextFieldFormEndDate: undefined,
    openSnackBarAddFeature: false,
    datePickerDateFeature: "",
  };

  handleDateForDateFeatures = (value) => {
    this.setState({ datePickerDateFeature: value._d.toISOString() });
  };

  handleDatePickerTextFieldFormStartDate = (value) => {
    this.setState({ datePickerTextFieldFormStartDate: value._d.toISOString() });
  };

  handleDatePickerTextFieldFormEndDate = (value) => {
    this.setState({ datePickerTextFieldFormEndDate: value._d.toISOString() });
  };

  handleFeatureChange = (event) => {
    let currentSelectedfeature = this.state.featuresList.find(
      (value) => value.description === event.target.value
    ).type;
    this.setState({ selectedFeature: event.target.value });
    this.setState({
      selectedFeatureType: this.state.featuresList.find(
        (value) => value.description === event.target.value
      ).type,
    });
    this.setState({
      selectedFeatureId: this.state.featuresList.find(
        (value) => value.description === event.target.value
      ).id,
    });
    // currentSelectedfeature === 'Form'? this.setState({selectedFeatureData : this.state.featuresList.find(value => value.description === event.target.value).defaultValue}): this.setState({selectedFeatureData: ''});
    currentSelectedfeature === "Form"
      ? this.setState({ selectedFeatureData: formData })
      : this.setState({ selectedFeatureData: "" });
  };

  handleFormChange = (value) => {
    console.log(value);
  };

  handleAddFeatureDialogOpen = async () => {
    let headers = new Headers();
    headers.append(
      "Authorization",
      `Bearer ${sessionStorage.getItem("token")}`
    );
    let params = {
      orderby: "id",
      ordertype: "asc",
      offset: 0,
      limit: 100,
      expression: '[{"Field":"parentid","FilterType":"Equal","Value":"6"}]',
    };

    let res = await DataProvider.GET("features/get", headers, params);
    if (typeof res === "undefined") return;
    this.setState({ featuresList: res.data });
  };

  handleCloseSnackbarAddFeature = () => {
    this.setState({ openSnackBarAddFeature: false });
  };

  handleAddFeatureClick = async (button) => {
    let res;
    let headers = new Headers();
    headers.append(
      "Authorization",
      `Bearer ${sessionStorage.getItem("token")}`
    );
    let formdata = new FormData();
    formdata.append("tableid", "Person");
    formdata.append("rowid", this.props.currentUser.id);
    formdata.append("featureid", this.state.selectedFeatureId);
    formdata.append(
      "startdate",
      new Date(this.state.datePickerTextFieldFormStartDate).toISOString()
    );
    formdata.append(
      "enddate",
      typeof this.state.datePickerTextFieldFormEndDate === "undefined"
        ? ""
        : this.state.datePickerTextFieldFormEndDate
    );
    switch (button) {
      case "cancel":
        this.setState({ addFeatureDialog: false });
        break;
      case "submit":
        switch (this.state.selectedFeatureType) {
          case "DateTime":
            formdata.append(
              "value",
              new Date(this.state.datePickerDateFeature).toISOString()
            );
            res = await DataProvider.POST(
              "features/assignfeature",
              headers,
              formdata
            );
            if (typeof res === "undefined") return;
            else if (res.id) console.log(res.id);
            this.setState({ addFeatureDialog: false });
            this.setState({ openSnackBarAddFeature: true });
            break;
          case "Form":
            console.log("process form");
            break;
          default:
            formdata.append("value", this.state.formTextInput);
            formdata.append(
              "startdate",
              new Date(
                this.state.datePickerTextFieldFormStartDate
              ).toISOString()
            );
            formdata.append(
              "enddate",
              typeof this.state.datePickerTextFieldFormEndDate === "undefined"
                ? ""
                : this.state.datePickerTextFieldFormEndDate
            );
            res = await DataProvider.POST(
              "features/assignfeature",
              headers,
              formdata
            );
            if (typeof res === "undefined") return;
            else if (res.id) console.log(res.id);
            this.setState({ addFeatureDialog: false });
            this.setState({ openSnackBarAddFeature: true });
            break;
        }

        this.setState({ addFeatureDialog: false });
        break;
      default:
    }
  };

  handleFormTextInputChange = (event) => {
    this.setState({ formTextInput: event.target.value });
  };

  handleTabPanelChange = (event, tabPanelValue) => {
    this.setState({ tabPanelValue });
  };

  createData(name, calories, fat, carbs, protein) {
    return { name, calories, fat, carbs, protein };
  }

  tempRows = [
    this.createData("Frozen yoghurt", 159, 6.0, 24, 4.0),
    this.createData("Ice cream sandwich", 237, 9.0, 37, 4.3),
    this.createData("Eclair", 262, 16.0, 24, 6.0),
    this.createData("Cupcake", 305, 3.7, 67, 4.3),
    this.createData("Gingerbread", 356, 16.0, 49, 3.9),
  ];

  addEvent = async () => {
    this.setState({ addFeatureDialog: true });
  };

  editEvent = () => {
    console.log("editEvent");
  };

  deleteEvent = () => {
    console.log("deleteEvent");
  };

  render() {
    const { classes } = this.props;
    return (
      <Fragment>
        <Dialog
          open={this.props.open}
          fullScreen
          maxWidth={false}
          className={classes.dialogRtl}
        >
          <AppBar className={classes.appBar}>
            <Toolbar>
              <Typography variant="h6" className={classes.title}>
                پروفایل کاربر
              </Typography>
              <Button
                autoFocus
                color="inherit"
                onClick={() => this.props.onCloseProfileDialog()}
              >
                <CloseIcon />
              </Button>
            </Toolbar>
          </AppBar>
          <DialogContent
            style={{
              marginTop: "100px",
            }}
          >
            <Grid container>
              <Grid
                container
                item
                style={{
                  backgroundColor: "#d7ecf9",
                  padding: "20px",
                }}
              >
                <Grid container item md={3} sm={12} justify="center">
                  <div
                    id="abed"
                    style={{
                      backgroundImage: 'url("profileImage.png")',
                      width: "200px",
                      height: "200px",
                      borderRadius: "50%",
                    }}
                  ></div>
                </Grid>
                <Grid container item md={9} sm={12}>
                  <Paper
                    elevation={3}
                    style={{
                      padding: "30px",
                    }}
                  >
                    <Grid>
                      نام: {this.props.currentUser.firstName} نام خانوادگی:{" "}
                      {this.props.currentUser.lastName} جنسیت:
                      {this.props.currentUser.gender === "Male" ? "مرد" : "زن"}{" "}
                      محل تولد: {this.props.currentUser.birthPlace}
                      <hr />
                      <br />
                      تاریخ تولد:{" "}
                      {new Date(
                        this.props.currentUser.birthDate
                      ).toLocaleString("fa-IR")}{" "}
                      تاریخ شروع:{" "}
                      {new Date(
                        this.props.currentUser.startDate
                      ).toLocaleString("fa-IR")}{" "}
                      تاریخ پایان:
                      {new Date(this.props.currentUser.endDate).toLocaleString(
                        "fa-IR"
                      )}
                    </Grid>
                  </Paper>
                </Grid>
              </Grid>
              <Grid container item justify="center">
                <AppBar position="static">
                  <Tabs
                    value={this.state.tabPanelValue}
                    onChange={this.handleTabPanelChange}
                    aria-label="simple tabs example"
                  >
                    <Tab label="اطلاعات ویژگی" {...a11yProps(0)} />
                    <Tab label="مستندات" {...a11yProps(1)} />
                    <Tab label="روابط" {...a11yProps(2)} />
                  </Tabs>
                </AppBar>
                <TabPanel value={this.state.tabPanelValue} index={0}>
                  <FAMToolbar
                    direction="ltr"
                    searchInput={true}
                    items={[
                      {
                        name: "اضافه کردن ویژگی",
                        icon: "fa fa-plus-square",
                        clickEvent: this.addEvent,
                      },
                      {
                        name: "ویرایش کردن ویژگی",
                        icon: "fa fa-edit",
                        clickEvent: this.editEvent,
                      },
                      {
                        name: "حذف کردن ویژگی",
                        icon: "fa fa-trash",
                        clickEvent: this.deleteEvent,
                      },
                    ]}
                  />
                  <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                      <TableHead>
                        <TableRow>
                          <TableCell>Dessert (100g serving)</TableCell>
                          <TableCell align="right">Calories</TableCell>
                          <TableCell align="right">Fat&nbsp;(g)</TableCell>
                          <TableCell align="right">Carbs&nbsp;(g)</TableCell>
                          <TableCell align="right">Protein&nbsp;(g)</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {this.tempRows.map((row) => (
                          <TableRow key={row.name}>
                            <TableCell component="th" scope="row">
                              {row.name}
                            </TableCell>
                            <TableCell align="right">{row.calories}</TableCell>
                            <TableCell align="right">{row.fat}</TableCell>
                            <TableCell align="right">{row.carbs}</TableCell>
                            <TableCell align="right">{row.protein}</TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </TabPanel>
                <TabPanel value={this.state.tabPanelValue} index={1}>
                  <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                      <TableHead>
                        <TableRow>
                          <TableCell>Dessert (100g serving)</TableCell>
                          <TableCell align="right">Calories</TableCell>
                          <TableCell align="right">Fat&nbsp;(g)</TableCell>
                          <TableCell align="right">Carbs&nbsp;(g)</TableCell>
                          <TableCell align="right">Protein&nbsp;(g)</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {this.tempRows.map((row) => (
                          <TableRow key={row.name}>
                            <TableCell component="th" scope="row">
                              {row.name}
                            </TableCell>
                            <TableCell align="right">{row.calories}</TableCell>
                            <TableCell align="right">{row.fat}</TableCell>
                            <TableCell align="right">{row.carbs}</TableCell>
                            <TableCell align="right">{row.protein}</TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </TabPanel>
                <TabPanel value={this.state.tabPanelValue} index={2}>
                  <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                      <TableHead>
                        <TableRow>
                          <TableCell>Dessert (100g serving)</TableCell>
                          <TableCell align="right">Calories</TableCell>
                          <TableCell align="right">Fat&nbsp;(g)</TableCell>
                          <TableCell align="right">Carbs&nbsp;(g)</TableCell>
                          <TableCell align="right">Protein&nbsp;(g)</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {this.tempRows.map((row) => (
                          <TableRow key={row.name}>
                            <TableCell component="th" scope="row">
                              {row.name}
                            </TableCell>
                            <TableCell align="right">{row.calories}</TableCell>
                            <TableCell align="right">{row.fat}</TableCell>
                            <TableCell align="right">{row.carbs}</TableCell>
                            <TableCell align="right">{row.protein}</TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </TableContainer>
                </TabPanel>
              </Grid>
            </Grid>
          </DialogContent>
        </Dialog>
        <Dialog
          open={this.state.addFeatureDialog}
          className={classes.dialogRtl}
          onEnter={this.handleAddFeatureDialogOpen}
        >
          <DialogTitle id="form-dialog-title">اضافه کردن ویژگی شخص</DialogTitle>
          <DialogContent>
            <DialogContentText></DialogContentText>
            <Grid container>
              <Grid container item md={6}>
                <InputLabel id="demo-simple-select-label">ویژگی</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={this.state.selectedFeature}
                  onChange={this.handleFeatureChange}
                >
                  {this.state.featuresList.map((value) => (
                    <MenuItem key={value.id} value={value.description}>
                      {value.description}
                    </MenuItem>
                  ))}
                </Select>
                {this.state.selectedFeatureType === "String" ||
                this.state.selectedFeatureType === "Int32" ||
                this.state.selectedFeatureType === "Double" ? (
                  <TextField
                    className={classes.mr2}
                    onChange={this.handleFormTextInputChange}
                  />
                ) : (
                  ""
                )}
                {this.state.selectedFeatureType === "DateTime" ? (
                  <DatePicker
                    isGregorian={false}
                    onChange={(value) => this.handleDateForDateFeatures(value)}
                  />
                ) : (
                  ""
                )}
              </Grid>
              <Grid container item md={6}>
                {this.state.selectedFeatureType === "String" ||
                this.state.selectedFeatureType === "Int32" ||
                this.state.selectedFeatureType === "Double" ||
                this.state.selectedFeatureType === "DateTime" ? (
                  <Fragment>
                    تاریخ شروع
                    <DatePicker
                      isGregorian={false}
                      onChange={(value) =>
                        this.handleDatePickerTextFieldFormStartDate(value)
                      }
                    />
                    تاریخ پایان
                    <DatePicker
                      isGregorian={false}
                      onChange={(value) =>
                        this.handleDatePickerTextFieldFormEndDate(value)
                      }
                    />
                  </Fragment>
                ) : (
                  ""
                )}
                {this.state.selectedFeatureData !== "" ? (
                  <Form
                    form={JSON.parse(this.state.selectedFeatureData)}
                    onChange={(value) => this.handleFormChange(value)}
                  />
                ) : (
                  ""
                )}
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button
              color="primary"
              onClick={() => this.handleAddFeatureClick("cancel")}
            >
              لغو
            </Button>
            <Button
              color="primary"
              onClick={() => this.handleAddFeatureClick("submit")}
            >
              ثبت
            </Button>
          </DialogActions>
        </Dialog>
        <Snackbar
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "left",
          }}
          open={this.state.openSnackBarAddFeature}
          autoHideDuration={1500}
          onClose={this.handleCloseSnackbarAddFeature}
          message="ویژگی اضافه شد"
          action={
            <React.Fragment>
              <IconButton size="small" aria-label="close" color="inherit">
                <CloseIcon fontSize="small" />
              </IconButton>
            </React.Fragment>
          }
        />
      </Fragment>
    );
  }
}

export default withStyles(myStyle)(ProfileDialogPerson);
