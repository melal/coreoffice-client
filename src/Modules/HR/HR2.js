import React, { Fragment,Component } from 'react';
import ReactDom from 'react-dom';
import MaterialTable from 'material-table';
import {Typography  ,Grid, IconButton, Tooltip,InputBase,Table,TableBody,TableCell,TableContainer,TableHead,TableRow,Paper,TableSortLabel,Checkbox,TablePagination,Toolbar,FormControlLabel,Switch    } from '@material-ui/core';
import { fade, makeStyles   } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import DeleteIcon from '@material-ui/icons/Delete';
import FilterListIcon from '@material-ui/icons/FilterList';
import { forwardRef } from 'react';

import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    p100: {
        paddingTop: '100px',
    },
    table:{
        direction: 'rtl',
        width: '100%',
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(1),
        width: 'auto',
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        width: '12ch',
        '&:focus': {
          width: '20ch',
        },
      },
    },
  }));

//   const columns = [
//     {
//      name: "name",
//      label: "نام",
//      options: {
//       filter: true,
//       sort: true,
//      }
//     },
//     {
//      name: "family",
//      label: "نام خانوادگی",
//      options: {
//       filter: true,
//       sort: false,
//      }
//     },
//     {
//      name: "fatherName",
//      label: "نام پدر",
//      options: {
//       filter: true,
//       sort: false,
//      }
//     },
//     {
//      name: "nationalCode",
//      label: "کدملی",
//      options: {
//       filter: true,
//       sort: false,
//      }
//     },
//     {
//      name: "birthday",
//      label: "تاریخ تولد",
//      options: {
//       filter: true,
//       sort: false,
//      }
//     },
//     {
//      name: "bornPlace",
//      label: "محل تولد",
//      options: {
//       filter: true,
//       sort: false,
//      }
//     },
//     {
//      name: "shenasnameCode",
//      label: "شماره شناسنامه",
//      options: {
//       filter: true,
//       sort: false,
//      }
//     },
//     {
//      name: "shenasnameSeries",
//      label: "سری شناسنامه",
//      options: {
//       filter: true,
//       sort: false,
//      }
//     },
//     {
//      name: "shenasnameSerial",
//      label: "سریال شناسنامه",
//      options: {
//       filter: true,
//       sort: false,
//      }
//     },
//     {
//      name: "sex",
//      label: "جنسیت",
//      options: {
//       filter: true,
//       sort: false,
//      }
//     },
//     {
//      name: "shenasnameSodoor",
//      label: "محل صدور",
//      options: {
//       filter: true,
//       sort: false,
//      }
//     },
//     {
//      name: "shenasnameDate",
//      label: "تاریخ صدور",
//      options: {
//       filter: true,
//       sort: false,
//      }
//     }
//    ];
   
//    const data = [
//     { name: "عبدا...", family: "علوی", fatherName: "علی", nationalCode: "0944900070" , birthday: "2000/02/23", bornPlace: "مشهد",shenasnameCode: "234", shenasnameSeries: "234321", shenasnameSerial: "12121", sex:"مرد", shenasnameSodoor : "مشهد", shenasnameDate: "200/12/03"},
//     { name: "علی", family: "فروردین", fatherName: "محمد", nationalCode: "0944900070" , birthday: "2000/02/23", bornPlace: "مشهد",shenasnameCode: "234", shenasnameSeries: "234321", shenasnameSerial: "12121", sex:"مرد", shenasnameSodoor : "مشهد", shenasnameDate: "200/12/03"},
//     { name: "محمد", family: "شهابی", fatherName: "رضا", nationalCode: "0944900070" , birthday: "2000/02/23", bornPlace: "مشهد",shenasnameCode: "234", shenasnameSeries: "234321", shenasnameSerial: "12121", sex:"مرد", shenasnameSodoor : "مشهد", shenasnameDate: "200/12/03"},
//     { name: "میلاد", family: "خان محمدی", fatherName: "علی", nationalCode: "0944900070" , birthday: "2000/02/23", bornPlace: "مشهد",shenasnameCode: "234", shenasnameSeries: "234321", shenasnameSerial: "12121", sex:"مرد", shenasnameSodoor : "مشهد", shenasnameDate: "200/12/03"},
//    ];

      
export default function HR(){
            const classes = useStyles();
            
    return ( 
        <Fragment >
                <Grid container justify="center" className={classes.p100}>
                    <Grid container item sm={10} xs={12} justify="space-between" direction="row-reverse" className="tableHeader">
                        <Grid container sm={4} xs={12} item justify="flex-start" direction="row-reverse" >
                                <Tooltip title={
                                    <Typography>
                                        اضافه کردن شخص حقیقی
                                    </Typography>
                                }>
                                    <IconButton>
                                        <AddBox />
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title={
                                    <Typography>
                                        ویرایش شخص حقیقی
                                    </Typography>
                                }>
                                    <IconButton>
                                        <Edit />
                                    </IconButton>
                                </Tooltip>
                                <Tooltip title={
                                    <Typography>
                                        حذف شخص حقیقی
                                    </Typography>
                                }>
                                    <IconButton>
                                        <DeleteIcon/>
                                    </IconButton>
                                </Tooltip>
                        </Grid>
                        <Grid container item sm={4} xs={12}>
                                <div className={classes.search}>
                                    <div className={classes.searchIcon}>
                                    <Search />
                                    </div>
                                    <InputBase
                                    placeholder="جستجو"
                                    classes={{
                                        root: classes.inputRoot,
                                        input: classes.inputInput,
                                    }}
                                    inputProps={{ 'aria-label': 'search' }}
                                    />
                                </div>
                        </Grid>
                    </Grid>
                    <Grid container item sm={10}>
                        <div style={{ maxWidth: "100%" }} className={classes.table}>
                            <MaterialTable
                            columns={[
                                { title: "Adı", field: "name" },
                                { title: "Soyadı", field: "surname" },
                                { title: "Doğum Yılı", field: "birthYear", type: "numeric" },
                                {
                                title: "Doğum Yeri",
                                field: "birthCity",
                                lookup: { 34: "İstanbul", 63: "Şanlıurfa" }
                                }
                            ]}
                            data={[
                                { name: "Mehmet", surname: "Baran", birthYear: 1987, birthCity: 63 }
                            ]}
                            title="Demo Title"
                            options={{
                                detailPanelColumnAlignment: "right"
                            }}
                            icons={tableIcons}
                            />
                        </div>
                    </Grid>
                </Grid>
        </Fragment>
    );
}
