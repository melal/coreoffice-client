import {DataProvider} from '../../DataProvider';

const IsAuthentication = async function ()
{
    let token = await DataProvider.Introspect();
    if (!token.active) {
        window.location.href="#/Authentication"
    }
    return token.active;
}

export default IsAuthentication;
