import React from "react";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { createMuiTheme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { Redirect } from "react-router-dom";
import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { addTokenToState, changeIsLoggedIn, populateMenu } from "../../actions";
import { connect } from "react-redux";
import { DataProvider } from "../../DataProvider";
import configs from "../../config/Config";
import "../../content/css/bootstrap.css";
import "../../content/css/MainContainer/Authentication/authentication.css";

class Authentication extends React.Component {
  state = {
    username: null,
    password: null,
    isUserLoggedIn: false,
    open: false,
    exceededLogin: false,
  };
  componentDidMount() {
    let token = sessionStorage.getItem("token");
    if (token)
      this.setState({
        isUserLoggedIn: true,
      });
  }

  doLogin = (e) => {
    let keyCode = e.keyCode;
    if (keyCode == "13") {
      this.loginUser();
    }
  };

  setUsername = (e) => {
    this.setState({
      username: e.target.value,
    });
  };

  savePassword = (e) => {
    this.setState({
      password: e.target.value,
    });
  };

  loginUser = async () => {
    // دریافت اطلاعات پیکر بندی برای احراز هویت
    let clientId = configs.AuthenticationConfig.ClientId;
    let clientSecret = configs.AuthenticationConfig.ClientSecret;
    let grantType = configs.AuthenticationConfig.GrantType;
    let scope = configs.AuthenticationConfig.Scope;

    let formData = new FormData();
    let userData = {
      client_id: clientId,
      client_secret: clientSecret,
      grant_type: grantType,
      scope: scope,
      username: this.state.username,
      password: this.state.password,
    };
    for (const name in userData) {
      formData.append(name, userData[name]);
    }

    let res = await DataProvider.POST("connect/token", {}, formData, 2);
    if (typeof res === `undefined`) return;
    if (res.access_token) {
      sessionStorage.setItem("token", res.access_token);
      sessionStorage.setItem("username", this.state.username);
      sessionStorage.setItem("password", this.state.password);
      sessionStorage.setItem("lastUsedTime", Date.now());
      sessionStorage.setItem("expires_length", res.expires_in * 1000);
      let introspectRes = await DataProvider.Introspect();
      sessionStorage.setItem("expires_in", introspectRes.exp * 1000);

      this.props.dispatch(
        addTokenToState({
          token: res.access_token,
        })
      );

      this.props.dispatch(
        changeIsLoggedIn({
          isLoggedIn: true,
        })
      );

      let headers = {
        authorization: `Bearer ${sessionStorage.getItem("token")}`,
      };

      let menuRes = await DataProvider.GET("configs/get", headers, {});
      if (menuRes.activePlugins) {
        this.props.dispatch(
          populateMenu({
            menu: menuRes.activePlugins,
          })
        );
      }
      this.setState({
        isUserLoggedIn: true,
      });
    }
  };
  handleCloseExceededLogin = () => {
    this.setState((state, props) => {
      return { exceededLogin: false };
    });
  };
  handleClose = () => {
    this.setState({
      open: false,
    });
  };
  render() {
    const theme = createMuiTheme();
    const classes = {
      // title: {
      //   display: "block",
      //   fontFamily: "IranSans",
      //   textAlign: "center",
      //   width: "100%",
      // },
      // main_signup: {
      //   padding: theme.spacing(4),
      //   direction: "rtl",
      // },
      // button: {
      //   fontFamily: "IranSans",
      //   fontSize: "14px",
      //   marginBottom: theme.spacing(2),
      // },
      // container_buttons: {
      //   display: "flex",
      //   flexDirection: "column",
      //   marginTop: theme.spacing(2),
      // },
    };

    if (this.state.isUserLoggedIn) {
      return <Redirect to="/Home" />;
    } else {
      return (
        <React.Fragment>
          <Dialog
            open={this.state.exceededLogin}
            onClose={this.handleCloseExceededLogin.bind(this)}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle
              id="form-dialog-title"
              style={{
                fontFmaily: "IranSans",
              }}
            >
              <Typography
                style={{
                  fontFmaily: "IranSans",
                }}
              >
                اوه یه مشکلی پیش اومد
              </Typography>
            </DialogTitle>
            <DialogContent
              style={{
                fontFmaily: "IranSans",
              }}
            >
              <Typography
                style={{
                  fontFmaily: "IranSans",
                }}
              >
                شما با یک مرورگر دیگر لاگین شده اید لطفا ابتدا از آن خارج شوید
              </Typography>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={this.handleCloseExceededLogin.bind(this)}
                color="primary"
                style={{
                  fontFmaily: "IranSans",
                }}
              >
                آها، متوجه شدم
              </Button>
            </DialogActions>
          </Dialog>
          <Snackbar
            open={this.state.open}
            autoHideDuration={2000}
            onClose={this.handleClose}
          >
            <Alert severity="warning">
              <Typography
                style={{
                  fontFamily: "IranSans",
                }}
              >
                کلمه عبور یا شماره تلفن را درست وارد نکردید
              </Typography>
            </Alert>
          </Snackbar>
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12 loginPanel">
              
              <Grid
                container
                justify="center"
                alignItems="center"
                spacing={0}
              >
                <Grid item xs={12} sm={8} md={8} lg={4} xl={4} className="mainPanel">
                  <Paper>
                    <Grid container spacing={0} >
                      <Typography className="title" >
                        ورود از طریق مشخصات کاربری
                      </Typography>
                      <TextField
                        id="standard-lastname"
                        label="نام کاربری"
                        fullWidth
                        margin="normal"
                        onKeyDown={this.doLogin}
                        onChange={this.setUsername}
                        InputProps={{
                          style: {
                            fontFamily: "IranSans",
                            fontSize: "14px",
                          },
                        }}
                        InputLabelProps={{
                          style: {
                            fontFamily: "IranSans",
                            fontSize: "14px",
                          },
                        }}
                      />
                    </Grid>

                    <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                      <TextField
                        id="standard-password"
                        label="کلمه عبور"
                        margin="normal"
                        fullWidth
                        onChange={this.savePassword}
                        onKeyDown={this.doLogin}
                        InputProps={{
                          style: {
                            fontFamily: "IranSans",
                            fontSize: "14px",
                          },
                        }}
                        InputLabelProps={{
                          style: {
                            fontFamily: "IranSans",
                            fontSize: "14px",
                          },
                        }}
                        type="password"
                      />
                    </Grid>

                    <div style={classes.container_buttons}>
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={this.loginUser}
                        className="btnLoginPanel"
                      >
                        ورود
                      </Button>

                      <Button href="#forget-password" style={classes.button}>
                        کلمه عبور خود را فراموش کرده اید؟
                      </Button>
                    </div>
                  </Paper>
                </Grid>
              </Grid>
            </div>
          </div>
        </React.Fragment>
      );
    }
  }
}

const mapStateToProps = (state) => {
  const custom_side_menu = state.custom_side_menu;
  return { custom_side_menu };
};

export default connect(mapStateToProps)(Authentication);
