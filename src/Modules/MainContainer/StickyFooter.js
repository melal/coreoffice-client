import React from "react";
import Grid from "@material-ui/core/Grid";
import { Link as RouterLink } from "react-router-dom";
import Link from "@material-ui/core/Link";
import ShoppingCart from "@material-ui/icons/ShoppingCart";
import LocalPhone from "@material-ui/icons/LocalPhone";
import Container from "@material-ui/core/Container";

//import Paper from '@material-ui/core/Paper';
//import Container from '@material-ui/core/Container';

class StickyFooter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      definitions: [],
      products: [],
    };
  }

  componentDidMount() {}

  render() {
    return (
      <div container="true">
        <div className="footer">
          <div className="context">
            تمامی حقوق مادی و معنوی این سامانه متعلق به شرکت تجارت الکترونیک و
            فناوری اطلاعات ملل می باشد
          </div>
        </div>
      </div>
    );
  }
}
export default StickyFooter;
