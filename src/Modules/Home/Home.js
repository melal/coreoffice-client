import React from "react";
import { Redirect } from "react-router-dom";
import IsAuthentication from "../MainContainer/AuthenticationTools";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

class Home extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      flag : false
    };
  }

  getResult = async() => {
    return await IsAuthentication()
  }

  async componentDidMount(){
    this.setState({
      flag: await this.getResult()
    });
  }

   render() {
     return(
       <div>{this.state.flag.toString()}</div>
     )
  }
}

export default Home;
