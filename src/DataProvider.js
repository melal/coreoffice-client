import configs from './config/Config';
/**
 * The DataProvider class has general methods for GET, POST and other helper method for requesting from web services.
 */
class DataProvider {
    /**
     * Use this function for GET request from a URL path.
     * @param {String} url - The path part of a url for GET request
     * @param {Object} header - The headers for GET request. It can be empty object.
     * @param {Object} params - The url parameters for GET request. It can be empty object or undefined.
     * @param {Number} APIType  - The Default is 1. For ServerApi use 1 or undefined, for Authentication Api use 2;
     */
    static async GET(url, header, params, APIType = 1) {
        // choose right API from config
        if (APIType === 1) {
            url = configs.ServerApi + url;
        } else if (APIType === 2) {
            url = configs.AuthenticationApi + url;
        }
        // adding parameters to url, if any
        if ((typeof params !== 'undefined') && (params.length !== 0)) {
            url += `?`;
            for (let p in params) {
                url += `${p}=${params[p]}&`
            }
        }
        url = url.substring(0, url.length - 1);
        var requestOptions = {
            method: 'GET',
            headers: header,
            redirect: 'follow'
        };
        let res = await fetch(url, requestOptions);
        console.log(`res.status: ${res.status}`);
        switch (res.status) {
            case 200:
                res = await res.json();
                return res;
            case 403:
                window.location.replace("#/accessdenied");
                break;
            case 401:
                /* 
                    TODO: 
                    it seems to have a formal solution instead of reload
                */
                sessionStorage.clear();
                window.location.href = "#/Authentication";
                window.location.reload();
                return;
                break;
            default:
                return;
        }
    }
    /**
     * Use this function for POST request from a URL path.
     * @param {String} url - The path part of a url for POST request
     * @param {Object} header - The headers for POST request. It can be empty object.
     * @param {Object} body - The body for POST request. It can be empty object or undefined.
     * @param {Number} APIType - The Default is 1. For ServerApi use 1 or undefined, for Authentication Api use 2;
     */
    static async POST(url, header, body, APIType = 1) {
        var requestOptions = {
            method: 'POST',
            headers: header,
            redirect: 'follow',
            body: body
        };
        // choose right API from config
        if (APIType === 1) {
            url = configs.ServerApi + url;
        } else if (APIType === 2) {
            url = configs.AuthenticationApi + url;
        }
        let res = await fetch(url, requestOptions);
        switch (res.status) {
            case 200:
            case 400:
                res = await res.json();
                return res;
            case 403:
                window.location.replace("#/accessdenied");
                break;
            case 401:
                window.location.replace("#/Authentication");
                sessionStorage.clear()
                return;
                break;
            default:
                return;
        }
    }
    /**
     * Use Introspect function to check a token whether is valid or not.
     */
    static async Introspect() {
        var myHeaders = new Headers();
        myHeaders.append("Authorization", `Basic ${btoa('api:secret')}`);
        var formdata = new FormData();
        formdata.append("token", sessionStorage.getItem('token'));
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: formdata,
            redirect: 'follow'
        };
        let url = configs.AuthenticationApi + "connect/introspect";
        let res = await fetch(url, requestOptions);
        res = await res.json();
        return res;
    }
    /**
     * User ConnectToken function to get a new token from web service.
     * @param {String} client_id 
     * @param {String} client_Secret 
     * @param {String} grant_type 
     * @param {String} scope 
     * @param {String} username 
     * @param {String} password 
     */
    static async ConnectToken(client_id, client_Secret, grant_type, scope, username, password) {
        let formData = new FormData();
        let userData = {
            client_id: 'webclient',
            client_secret: 'secret',
            grant_type: 'password',
            scope: 'api',
            username: username,
            password: password,
        }
        for (const name in userData) {
            formData.append(name, userData[name]);
        }
        let res = await this.POST('connect/token', {}, formData, 2);
		return res;
    }
}
export {
    DataProvider
}