# Contributing

So you want to contribute to coreoffice? Awesome! Thank you in advance for your contribution. Here are a few guidelines that will help you along the way.

## Asking Questions

For how-to questions and other non-issues, please use [question] label on issues. 
## Opening an Issue

If you think you have found a bug, or have a new feature idea, please start by making sure it hasn't already been [reported or fixed](https://gitlab.com/miladkhan/coreoffice/-/issues?scope=all&utf8=%E2%9C%93&state=closed). You can search through existing issues and PRs to see if someone has reported one similar to yours.

Next, create a new issue that briefly explains the problem, and provides a bit of background as to the circumstances that triggered it, and steps to reproduce it.

For code issues please include:
* coreoffice version
* React version
* Browser version
* A code example or link to a repo, gist or running site. (hint: fork [this sandbox](https://codesandbox.io/s/github/marmelab/react-admin/tree/master/examples/simple) to create a reproducible version of your bug)

For visual or layout problems, images or animated gifs can help explain your issue.
It's even better with a live reproduction test case.

### Issue Guidelines

Please use a succinct description. "doesn't work" doesn't help others find similar issues.

Please don't group multiple topics into one issue, but instead each should be its own issue.

And please don't just '+1' an issue. It spams the maintainers and doesn't help move the issue forward.

## Submitting a Merge Request

coreoffice is a community project, so pull requests are always welcome, but before working on a large change, it is best to open an issue first to discuss it with the maintainers. In that case, prefix it with "[RFC]" (Request for Comments)

When in doubt, keep your pull requests small. To give a PR the best chance of getting accepted, don't bundle more than one feature or bug fix per pull request. It's always best to create two smaller PRs than one big one.

The core team prefix their PRs width "[WIP]" (Work in Progress) or "[RFR]" (ready for Review), don't hesitate to do the same to explain how far you are from completion.

When adding new features or modifying existing, please attempt to include tests to confirm the new behaviour.

## How to write components and adding CSS 

coreoffice is a community project, so pull requests are always welcome, but before working on a large change, it is best to open an issue first to discuss it with the maintainers. In that case, prefix it with "[RFC]" (Request for Comments)

When in doubt, keep your pull requests small. To give a PR the best chance of getting accepted, don't bundle more than one feature or bug fix per pull request. It's always best to create two smaller PRs than one big one.

The core team prefix their PRs width "[WIP]" (Work in Progress) or "[RFR]" (ready for Review), don't hesitate to do the same to explain how far you are from completion.

When adding new features or modifying existing, please attempt to include tests to confirm the new behaviour.

## Commits and Push rules 

coreoffice is a community project, so pull requests are always welcome, but before working on a large change, it is best to open an issue first to discuss it with the maintainers. In that case, prefix it with "[RFC]" (Request for Comments)

When in doubt, keep your pull requests small. To give a PR the best chance of getting accepted, don't bundle more than one feature or bug fix per pull request. It's always best to create two smaller PRs than one big one.

The core team prefix their PRs width "[WIP]" (Work in Progress) or "[RFR]" (ready for Review), don't hesitate to do the same to explain how far you are from completion.

When adding new features or modifying existing, please attempt to include tests to confirm the new behaviour.

### Patches or hotfix: PR on `master`

Bug fixes that don't break existing apps can be PRed against `master`. Hotfix versions are released usually within days. 

### New Features or BC breaks: PR on `next`

At any given time, `next` represents the latest development version of the library. It is merged to master and released on a monthly basis.

## React Warnings and errors in console output 
Resolve all errors and warnings in console output

## Avoid writing codes in Typescript
Avoid writing codes in typescript, coffescript,etc. any language that needs transpling to pure javascript.

## Javascript version
you are free to use any version of javascript [ES6, ES7, ES8], webpack transpiles them to ES6, in compile and build process.

## New Routs
if you genarate a rout in program add it inside [ROUTES.md](https://gitlab.com/miladkhan/coreoffice/-/blob/master/ROUTES.md) file

## Coding style

You must follow the coding style of the existing files. coreoffice uses eslint and [prettier](https://github.com/prettier/prettier). You can reformat all the project files automatically by calling

```sh
make prettier
```

**Tip**: If possible, enable linting in your editor to get realtime feedback and/or fixes.


## Git Commit Message Template 

Please use the following guidelines to format all your commit     
messages:

     <type>(<scope>): <subject>
     <BLANK LINE>
     <body>
     <BLANK LINE>
     <footer>
 
 Please note that:
  - The HEADER is a single line of max. 50 characters that         
    contains a succinct description of the change. It contains a   
    type, an optional scope, and a subject
       + <type> describes the kind of change that this commit is   
                providing. Allowed types are:
             * feat (feature)
             * fix (bug fix)
             * docs (documentation)
             * style (formatting, missing semicolons, …)
             * refactor
             * test (when adding missing tests)
             * chore (maintain)
       + <scope> can be anything specifying the place of the commit    
                 change
       + <subject> is a very short description of the change, in   
                   the following format:
             * imperative, present tense: “change” not             
               “changed”/“changes”
             * no capitalised first letter
             * no dot (.) at the end
  - The BODY should include the motivation for the change and      
    contrast this with previous behavior and must be phrased in   
    imperative present tense 
  - The FOOTER should contain any information about Breaking       
    Changes and is also the place to reference GitHub issues that   
    this commit closes


## License

By contributing your code to the marmelab/react-admin GitHub repository, you agree to license your contribution under the FAM license.